import { Canvas } from "./Canvas";
import img from "./assets/*.png";

export default interface Texture {
	readonly texture: HTMLImageElement;
}

export const dirt: Texture = {
	texture: Canvas.loadImage(img.dirt),
};

export const grass: Texture = {
	texture: Canvas.loadImage(img.grass),
};

export const stone: Texture = {
	texture: Canvas.loadImage(img.stone),
};

export const iceTop: Texture = {
	texture: Canvas.loadImage(img.iceTop),
};

export const ice: Texture = {
	texture: Canvas.loadImage(img.ice),
};

export const stair_bottomLeft: Texture = {
	texture: Canvas.loadImage(img.stair_bottomLeft),
};

export const stair_bottomRight: Texture = {
	texture: Canvas.loadImage(img.stair_bottomRight),
};

export const stair_topLeft: Texture = {
	texture: Canvas.loadImage(img.stair_topLeft),
};

export const stair_topRight: Texture = {
	texture: Canvas.loadImage(img.stair_topRight),
};
