import { Entity } from "./Entity";
import { Canvas } from "./Canvas";
import { Camera } from "./Camera";
import { Position, Size } from "./Data";
import { Input } from "./Input";
import { Block } from "./blocks/Block";
import { Air } from "./blocks/Air";
import { Water } from "./blocks/Water";
import { Liquid } from "./Liquid";
import { Chunk } from "./Chunk";
import { Rectangle, Shape } from "./Shape";

export class Player extends Entity {
	private name: string;
	private speed = 4;
	private jump = 8;
	private input: Input;
	public fly = false;
	public noclip = false;
	readonly hitbox: Rectangle;

	constructor(name: string, size: Size, position: Position) {
		super(position, size);
		this.name = name;
		/*this.hitbox.push({ from: { x: 0, y: size.y }, to: { x: size.x, y: size.y } });
		this.hitbox.push({ from: { x: size.x, y: size.y }, to: { x: size.x, y: 0 } });
		this.hitbox.push({ from: { x: size.x, y: 0 }, to: { x: 0, y: 0 } });
		this.hitbox.push({ from: { x: 0, y: 0 }, to: { x: 0, y: size.y } });*/
		this.hitbox = new Rectangle({ x: 0, y: 0 }, { w: size.w, h: size.h });

		this.chunks = { left: this.getLeftChunk(), right: this.getRightChunk() };
	}

	public getLeftChunk(): number {
		return Math.floor(this.position.x);
	}

	public getRightChunk(): number {
		return Math.floor(this.position.x + this.size.w);
	}

	public getMiddleChunk(): number {
		return Math.floor(this.position.x + this.size.w / 2);
	}

	/*public relativeHitbox(pos: Position): Shape[] {
		let out: Shape[] = [];
		this.hitbox.forEach((shape) => {
			out.push(shape.relative(pos));
		});
		return out;
	}*/

	public addPosition(pos: Position): Position {
		// ! Remove  - this.size.h for rectangles, keep for polygons
		return { x: this.position.x + pos.x, y: this.position.y + pos.y };
	}

	/*public relativeHitbox(hitbox: Segment[]): Segment[] {
		let out: Segment[] = [];
		hitbox.forEach((segment) => {
			out.push({
				from: { x: segment.from.x + this.position.x, y: -segment.from.y + this.position.y },
				to: { x: segment.to.x + this.position.x, y: -segment.to.y + this.position.y },
			});
		});
		return out;
	}*/

	public setInput(input: Input): void {
		this.input = input;
	}

	public left(distance: number): void {
		for (let i = 0; i < distance; i++) {
			if (
				this.noclip ||
				!this.dimension.checkSolidPixelScope(this.floorRange(this.getLeftRange()), this.hitbox.relative(this.addPosition({ x: -0.01, y: 0 })))
			) {
				this.position.x -= 0.01; // Todo: better solution
			}
		}
	}

	public right(distance: number): void {
		for (let i = 0; i < distance; i++) {
			if (
				this.noclip ||
				!this.dimension.checkSolidPixelScope(this.floorRange(this.getRightRange()), this.hitbox.relative(this.addPosition({ x: 0.01, y: 0 })))
			) {
				this.position.x += 0.01;
			}
		}
	}

	public up(distance: number): void {
		for (let i = 0; i < distance; i++) {
			if (
				this.noclip ||
				!this.dimension.checkSolidPixelScope(this.floorRange(this.getTopRange()), this.hitbox.relative(this.addPosition({ x: 0, y: 0.01 })))
			) {
				this.position.y += 0.01;
			}
		}
	}

	public down(distance: number): void {
		for (let i = 0; i < distance; i++) {
			if (
				this.noclip ||
				!this.dimension.checkSolidPixelScope(this.floorRange(this.getBottomRange()), this.hitbox.relative(this.addPosition({ x: 0, y: -0.01 })))
			) {
				this.position.y -= 0.01;
			}
		}
	}

	public getPos(): Position {
		return this.position;
	}

	public getMiddlePos(): Position {
		return {
			x: Math.floor(this.position.x + this.size.w / 2),
			y: Math.floor(this.position.y + this.size.h / 2),
		};
	}

	public getBottomRange(): { from: Position; to: Position } {
		return {
			from: { x: this.position.x, y: this.position.y - 0.01 },
			to: { x: this.position.x + this.size.w, y: this.position.y - 0.01 },
		};
	}

	public getRightRange(): { from: Position; to: Position } {
		return {
			from: { x: this.position.x + this.size.w + 0.01, y: this.position.y },
			to: { x: this.position.x + this.size.w + 0.01, y: this.position.y + this.size.h },
		};
	}

	public getLeftRange(): { from: Position; to: Position } {
		return {
			from: { x: this.position.x - 0.01, y: this.position.y },
			to: { x: this.position.x - 0.01, y: this.position.y + this.size.h },
		};
	}

	public getTopRange(): { from: Position; to: Position } {
		return {
			from: { x: this.position.x, y: this.position.y + this.size.h + 0.01 },
			to: { x: this.position.x + this.size.w, y: this.position.y + this.size.h + 0.01 },
		};
	}

	public floorRange(range: { from: Position; to: Position }): { from: Position; to: Position } {
		return {
			from: { x: Math.floor(range.from.x), y: Math.floor(range.from.y) },
			to: { x: Math.floor(range.to.x), y: Math.floor(range.to.y) },
		};
	}

	public setPos(pos: Position): void {
		this.position = pos;
	}

	public setMiddlePos(position: Position): void {
		this.position = { x: position.x - this.size.w / 2, y: position.y + this.size.h / 2 + 0.01 };
	}

	public placeBlock(block: any): void {
		//console.log("place");
		//this.chunk.setBlock(block, this.input.getMouseBlockCoordonate());

		//block.position = this.input.getMouseBlockCoordonate();
		//block.chunk = this.chunk;

		const position: Position = this.input.getMouseBlock();
		//const chunk: Chunk = this.dimension.getChunkById(position.x);
		if (block == Water) {
			block = new Water(position, this.dimension, new Liquid());
		} else {
			console.log("placing");
			block = new block(position, this.dimension, this.input.getMousePositionInsideBlock());
		}

		const pointingAt: Block = this.getBlock();

		if (pointingAt instanceof Air || (pointingAt instanceof Water && !(block instanceof Water))) {
			this.dimension.setBlock(block);
		} else if (pointingAt instanceof Water && block instanceof Water) {
			if (pointingAt.liquid.volume + 100 <= pointingAt.liquid.quantity * 100) {
				pointingAt.liquid.volume += 100;
			} else {
				// Todo: place when what you place is bellow 100 ?
				pointingAt.liquid.volume = pointingAt.liquid.quantity * 100;
			}
		}
	}

	public getBlock(): Block {
		return this.dimension.getBlock(this.input.getMouseBlock());
	}

	public update(): void {
		const touchFloor: boolean = this.dimension.checkSolidPixelScope(
			this.floorRange(this.getBottomRange()),
			this.hitbox.relative(
				this.addPosition({ x: 0, y: -0.01 })
			) /*, [this.getLeftRange(), this.getRightRange(), this.getTopRange(), this.getBottomRange()]*/
		);
		const touchRoof: boolean = this.dimension.checkSolidPixelScope(
			this.floorRange(this.getTopRange()),
			this.hitbox.relative(this.addPosition({ x: 0, y: 0.01 }))
		);

		const touchLeft = this.dimension.checkSolidPixelScope(
			this.floorRange(this.getLeftRange()),
			this.hitbox.relative(this.addPosition({ x: -0.01, y: 0 }))
		);
		const touchRight = this.dimension.checkSolidPixelScope(
			this.floorRange(this.getRightRange()),
			this.hitbox.relative(this.addPosition({ x: 0.01, y: 0 }))
		);

		if (this.fly) {
			this.acceleration = { x: 0, y: 0 };
			this.velocity = { x: 0, y: 0 };
			if (this.input.left) this.acceleration.x -= 150;
			if (this.input.up) this.acceleration.y += 150;
			if (this.input.right) this.acceleration.x += 150;
			if (this.input.down) this.acceleration.y -= 150;
		} else/* if (touchFloor)*/ {
			
			
			const gravity = 0.5;
			this.acceleration.y -= gravity;
			if(touchFloor) {
				let friction: number = this.dimension.checkFriction(this.floorRange(this.getBottomRange()), this.hitbox.relative(this.addPosition({ x: 0, y: -0.01 })));
				//this.acceleration = {x: this.acceleration.x*(1-friction), y: this.acceleration.y - this.velocity.y};
				this.acceleration.x = this.acceleration.x*(1-friction);
				//if(this.acceleration.y < 0) this.acceleration.y = 0;
				//this.velocity = {x: this.velocity.x*(1-friction), y: this.velocity.y};
			}
			else {
				
			}

			if (touchFloor && this.acceleration.y < 0) {
				const bounciness: number = this.dimension.checkBounciness(
					this.floorRange(this.getBottomRange()),
					this.hitbox.relative(this.addPosition({ x: 0, y: -0.01 }))
				);
				//console.log(friction + " " + bounciness);
				this.acceleration.y = this.acceleration.y* (-bounciness);
				this.velocity.y = this.velocity.y* (-bounciness);
				//this.velocity = { x: this.velocity.x, y: this.velocity.y * -bounciness };
			}
			if (touchRoof && this.velocity.y > 0) {
				const bounciness: number = this.dimension.checkBounciness(
					this.floorRange(this.getTopRange()),
					this.hitbox.relative(this.addPosition({ x: 0, y: 0.01 }))
				);
				this.acceleration = { x: this.acceleration.x, y: 0 };
				this.velocity = { x: this.velocity.x, y: this.velocity.y * -bounciness };
			}
			if (touchLeft && this.velocity.x < 0) {
				//this.acceleration = {x: 0, y: this.acceleration.y}
				const bounciness: number = this.dimension.checkBounciness(
					this.floorRange(this.getLeftRange()),
					this.hitbox.relative(this.addPosition({ x: -0.01, y: 0 }))
				);
				//console.log(bounciness);
				this.acceleration = { x: 0, y: this.acceleration.y };
				this.velocity = { x: this.velocity.x * -bounciness, y: this.velocity.y };
			}
			if (touchRight && this.velocity.x > 0) {
				const bounciness: number = this.dimension.checkBounciness(
					this.floorRange(this.getRightRange()),
					this.hitbox.relative(this.addPosition({ x: 0.01, y: 0 }))
				);
				this.acceleration = { x: 0, y: this.acceleration.y };
				this.velocity = { x: this.velocity.x * -bounciness, y: this.velocity.y };
			}

			if (touchFloor) {
				if (this.input.left) this.acceleration.x -= this.speed;
				if (this.input.up) this.acceleration.y += this.jump;
				if (this.input.right) this.acceleration.x += this.speed;
				if (this.input.down) this.acceleration.y -= this.speed;
			} else {
				if (this.input.left) this.acceleration.x -= this.speed * 0.1;
				if (this.input.right) this.acceleration.x += this.speed * 0.1;
			}

			if (this.velocity.x < 0.01 && this.velocity.x > -0.01) {
				this.velocity.x = 0;
			}
			if (this.velocity.y < 0.01 && this.velocity.y > -0.01) {
				this.velocity.y = 0;
			}
			if (this.acceleration.x < 0.01 && this.acceleration.x > -0.01) {
				this.acceleration.x = 0;
			}
			if (this.acceleration.y < 0.01 && this.acceleration.y > -0.01) {
				this.acceleration.y = 0;
			}
		} /* else {
			this.acceleration = { x: this.acceleration.x * 0.5, y: this.acceleration.y * 0.5 };

			if (this.input.left) this.acceleration.x -= this.speed * 0.1;
			if (this.input.right) this.acceleration.x += this.speed * 0.1;
		}*/

		

		const drag: number = this.dimension.checkDrag(this.hitbox.relative(this.position));
		//console.log(drag);
		this.acceleration = { x: this.acceleration.x * (1 - drag), y: this.acceleration.y * (1 - drag) };

		this.velocity.x += this.acceleration.x;
		this.velocity.y += this.acceleration.y;

		if (this.velocity.x >= 0) {
			this.right(this.velocity.x);
		} else {
			this.left(-this.velocity.x);
		}

		if (this.velocity.y >= 0) {
			this.up(this.velocity.y);
		} else {
			this.down(-this.velocity.y);
		}

		if (this.getLeftChunk() != this.chunks.left) {
			if (this.getLeftChunk() < this.chunks.left) {
				//console.log(this.getLeftChunk() + " " + Math.max(this.chunks.left, this.getLeftChunk()));
				for (let i: number = this.getLeftChunk(); i <= this.getRightChunk(); i++) {
					//console.log("added chunk " + i);
					this.dimension.getChunk(i).addEntity(this);
				}
			}
			if (this.getLeftChunk() > this.chunks.left) {
				//console.log(this.chunks.left + " " + this.getLeftChunk());
				for (let i: number = this.chunks.left; i < this.getLeftChunk(); i++) {
					//console.log("removed chunk " + i);
					this.dimension.getChunk(i).removeEntity(this);
				}
			}
			this.chunks.left = this.getLeftChunk();
		}

		if (this.getRightChunk() != this.chunks.right) {
			if (this.getRightChunk() > this.chunks.right) {
				//console.log(Math.max(this.chunks.right, this.getRightChunk()) + " " + this.getRightChunk());
				for (let i: number = this.getLeftChunk(); i <= this.getRightChunk(); i++) {
					//console.log("added chunk " + i);
					this.dimension.getChunk(i).addEntity(this);
				}
			}
			if (this.getRightChunk() < this.chunks.right) {
				//console.log(this.getRightChunk()+1 + " " + this.chunks.right);
				for (let i: number = this.getRightChunk() + 1; i <= this.chunks.right; i++) {
					//console.log("removed chunk " + i);
					this.dimension.getChunk(i).removeEntity(this);
				}
			}
			this.chunks.right = this.getRightChunk();
		}
	}

	public isVisible(camera: Camera, size: Size): boolean {
		return (
			this.position.x * size.w + this.size.w >= camera.pos.x &&
			-this.position.y * size.h + this.size.h >= camera.pos.y &&
			this.position.x * size.w <= camera.pos.x + camera.size.w &&
			-this.position.y * size.h <= camera.pos.y + camera.size.h
		);
	}

	public removeBlock(): void {
		this.dimension.removeBlock(this.input.getMouseBlock());
	}

	// Todo: put all render to 1
	public render(canvas: Canvas, camera: Camera, size: Size): void {
		//console.log((this.pos.x + this.size.x >= cam.pos.x) + " " + (this.pos.y + this.size.y >= cam.pos.y) + " " + (this.pos.x <= cam.pos.x + cam.size.x) + " " + (this.pos.y <= cam.pos.y + cam.size.y));
		canvas.fill("blue");
		canvas.fillRect(
			{ x: this.position.x * size.w - camera.pos.x, y: -(this.position.y + this.size.h - 1) * size.h - camera.pos.y },
			{ w: this.size.w * size.w, h: this.size.h * size.h }
		);

		canvas.fill("purple");
		canvas.fillRect(this.input.getMouseBlockPos(this.dimension.getCamera(), size), size);
		// hitbox
		/*canvas.stroke("red");
		let seg: Segment = this.getRightRange();
		let seg1: Segment = {from: {x: seg.from.x *size.x - camera.pos.x, y: -(seg.from.y-1)* size.y - camera.pos.y}, to: {x: seg.to.x *size.x - camera.pos.x, y: -(seg.to.y-1)* size.y - camera.pos.y}};
		canvas.line(seg1);*/

		canvas.stroke("Red");
		//this.hitbox.forEach((shape) => {
		this.hitbox.draw(canvas, { x: this.position.x, y: this.position.y }, camera, size);
		//});

		canvas.fill("black");
		const pos: Position = this.input.getMouseBlock();
		const biomeSize =
			Math.abs(this.dimension.getChunk(this.getMiddleChunk()).biome.scope.to) -
			Math.abs(this.dimension.getChunk(this.getMiddleChunk()).biome.scope.from);
		canvas.text("Player position : " + Math.floor(this.position.x) + " " + Math.floor(this.position.y), { x: 100, y: 10 });
		canvas.text("Player acceleration : " + this.acceleration.x + " " + this.acceleration.y, { x: 100, y: 30 });
		canvas.text("Player velocity : " + this.velocity.x + " " + this.velocity.y, { x: 100, y: 50 });
		canvas.text("Player chunk : " + this.chunks.left + " " + this.chunks.right, { x: 100, y: 70 });
		let coordInsideBlock = this.input.getMousePositionInsideBlock(camera, size);
		canvas.text("Mouse position : " + pos.x + " " + pos.y + " " + coordInsideBlock.x + " " + coordInsideBlock.y, { x: 100, y: 90 });
		canvas.text(
			"Biome : " +
				this.dimension.getChunk(this.getMiddleChunk()).biome.name +
				" " +
				this.dimension.getChunk(this.getMiddleChunk()).biome.id +
				" ; Size : " +
				biomeSize,
			{ x: 100, y: 110 }
		);

		/*const bottom: { from: Position; to: Position } = this.getBottomBlocks(size);
		canvas.text(1, "bottom " + bottom.from.x + " " + bottom.from.y + " " + this.chunk.checkSolidScope(bottom), { x: 100, y: 50 });
		canvas.text(1, "bottom " + bottom.to.x + " " + bottom.to.y, { x: 100, y: 70 });

		const left: { from: Position; to: Position } = this.getLeftBlocks(size);
		canvas.text(1, "left " + left.from.x + " " + left.from.y + " " + this.chunk.checkSolidScope(left), { x: 100, y: 90 });
		canvas.text(1, "left " + left.to.x + " " + left.to.y, { x: 100, y: 110 });

		const right: { from: Position; to: Position } = this.getRightBlocks(size);
		canvas.text(1, "right " + right.from.x + " " + right.from.y + " " + this.chunk.checkSolidScope(right), { x: 100, y: 130 });
		canvas.text(1, "right " + right.to.x + " " + right.to.y, { x: 100, y: 150 });

		const top: { from: Position; to: Position } = this.getTopBlocks(size);
		canvas.text(1, "top " + top.from.x + " " + top.from.y + " " + this.chunk.checkSolidScope(top), { x: 100, y: 170 });
		canvas.text(1, "top " + top.to.x + " " + top.to.y, { x: 100, y: 190 });*/
	}
}
