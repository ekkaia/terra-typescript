import { Block } from "./blocks/Block";
import { Air } from "./blocks/Air";
import { Camera } from "./Camera";
import { Canvas } from "./Canvas";
import { Chunk } from "./Chunk";
import { Position, Size } from "./Data";
import { Entity } from "./Entity";
import { Player } from "./Player";
import Noise from "./Noise";
import { Biome } from "./biomes/Biome";
import { Plains } from "./biomes/Plains";
import { Snow } from "./biomes/Snow";
import { Rectangle, Shape } from "./Shape";
import { NonFullBlock } from "./blocks/NonFullBlock";

export class Dimension {
	private name: string;
	public map: Chunk[] = [];
	public spawn: number;
	private camera: Camera;
	private size: Size; // block size
	private entities: Entity[] = [];
	private biomeNoiseRight: Noise;
	private biomeNoiseLeft: Noise;
	private needsUpdate: Block[];
	private nextUpdate: Block[];
	private nextRightBiomePos = 0;
	private nextRightBiome: Biome;
	private nextLeftBiomePos = 0;
	private nextLeftBiome: Biome;
	private rightId = 0;
	private leftId = 0;

	constructor(size: Size) {
		// generate basic world
		this.name = "overworld";
		this.spawn = 0;
		this.needsUpdate = [];
		this.nextUpdate = [];
		this.biomeNoiseRight = new Noise(0, 1, 30); // -0.5 à 0.5
		this.biomeNoiseLeft = new Noise(0, 1, 30); // -0.5 à 0.5
		this.map.push(new Chunk(this, 0, this.chooseBiomeRight(this.leftId)));
		this.camera = new Camera();
		this.size = size;
	}

	public checkDrag(hitbox: Rectangle): number {
		let drag = 0;

		for (let x: number = Math.floor(hitbox.pos.x); x <= Math.floor(hitbox.pos.x + hitbox.size.w); x++) {
			for (let y: number = Math.floor(hitbox.pos.y); y <= Math.floor(hitbox.pos.y + hitbox.size.h); y++) {
				const block = this.getBlock({ x: x, y: y });
				if (!(block instanceof NonFullBlock) || block.check(hitbox)) {
					//console.log('ok: ' + block.position.x + " " + block.position.y + " " + block.drag)
					drag = Math.max(drag, block.drag);
				}
			}
		}
		return drag;
	}

	checkFriction(segment: { from: Position; to: Position }, hitbox: Rectangle): number {
		const frictions: number[] = [];

		for (let x: number = Math.floor(segment.from.x); x <= Math.floor(segment.to.x); x++) {
			const block = this.getBlock({ x: x, y: Math.floor(segment.from.y) });
			if (block.isSolid && (!(block instanceof NonFullBlock) || block.check(hitbox))) {
				frictions.push(block.friction);
			}
		}

		let friction = 0;
		if (frictions.length > 0) {
			frictions.forEach((f) => {
				friction += f;
			});
			friction /= frictions.length;
		}
		return friction;
	}

	checkBounciness(scope: { from: Position; to: Position }, hitbox: Rectangle): number {
		const bounciness: number[] = [];

		if (scope.from.x == scope.to.x) {
			for (let y: number = Math.floor(scope.from.y); y <= Math.floor(scope.to.y); y++) {
				const block = this.getBlock({ x: Math.floor(scope.from.x), y: y });
				if (block.isSolid && (!(block instanceof NonFullBlock) || block.check(hitbox))) {
					bounciness.push(block.bounciness);
				}
			}
		} else {
			for (let x: number = Math.floor(scope.from.x); x <= Math.floor(scope.to.x); x++) {
				const block = this.getBlock({ x: x, y: Math.floor(scope.from.y) });
				if (block.isSolid && (!(block instanceof NonFullBlock) || block.check(hitbox))) {
					bounciness.push(block.bounciness);
				}
			}
		}

		let bounce = 0;
		if (bounciness.length > 0) {
			bounciness.forEach((f) => {
				bounce += f;
			});
			bounce /= bounciness.length;
		}
		return bounce;
	}

	public zoom(i: number): void {
		this.size.w = this.size.w + i || 1;
		this.size.h = this.size.h + i || 1;
	}

	public checkSolidPixelScope(scope: { from: Position; to: Position }, hitbox: Shape): boolean {
		let isSolid = false;
		// const emptySpace = { from: { x: scope.from.x, y: scope.from.y }, to: { x: scope.to.x, y: scope.to.y } };
		if (scope.from.x == scope.to.x) {
			// vertical
			for (let i: number = scope.from.y; i <= scope.to.y; i++) {
				const block: Block = this.getBlock({ x: scope.from.x, y: i });
				if (block.isSolid && (!(block instanceof NonFullBlock) || block.check(hitbox))) {
					isSolid = true;
				}
			}
		} else {
			// horizontal
			for (let i: number = scope.from.x; i <= scope.to.x; i++) {
				const block: Block = this.getBlock({ x: i, y: scope.from.y });
				if (block.isSolid && (!(block instanceof NonFullBlock) || block.check(hitbox))) {
					isSolid = true;
				}
			}
		}
		return isSolid;
	}

	public chooseBiomeLeft(beginPosition: number): Biome {
		let endPosition = beginPosition;
		let noiseValue = this.biomeNoiseLeft.generate(beginPosition);
		let newBiome: Biome;
		if (noiseValue >= 0) {
			while (noiseValue >= 0) {
				endPosition -= 1;
				noiseValue = this.biomeNoiseLeft.generate(endPosition);
			}
			this.nextLeftBiomePos = endPosition;
			this.nextLeftBiome = new Snow({ from: endPosition, to: undefined }, 40);
			endPosition += 1;
			newBiome = new Plains({ from: beginPosition, to: endPosition }, 40);
		} else {
			while (noiseValue < 0) {
				endPosition -= 1;
				noiseValue = this.biomeNoiseLeft.generate(endPosition);
			}
			this.nextLeftBiomePos = endPosition;
			this.nextLeftBiome = new Plains({ from: endPosition, to: undefined }, 40);
			endPosition += 1;
			newBiome = new Snow({ from: beginPosition, to: endPosition }, 40);
		}
		return newBiome;
	}

	public chooseBiomeRight(beginPosition: number): Biome {
		let endPosition = beginPosition;
		let noiseValue = this.biomeNoiseRight.generate(beginPosition);
		let newBiome: Biome;
		if (noiseValue >= 0) {
			while (noiseValue >= 0) {
				endPosition += 1;
				noiseValue = this.biomeNoiseRight.generate(endPosition);
			}
			this.nextRightBiomePos = endPosition;
			this.nextRightBiome = new Snow({ from: endPosition, to: undefined }, 40);
			endPosition -= 1;
			newBiome = new Plains({ from: beginPosition, to: endPosition }, 40);
		} else {
			while (noiseValue < 0) {
				endPosition += 1;
				noiseValue = this.biomeNoiseRight.generate(endPosition);
			}
			this.nextRightBiomePos = endPosition;
			this.nextRightBiome = new Plains({ from: endPosition, to: undefined }, 40);
			endPosition -= 1;
			newBiome = new Snow({ from: beginPosition, to: endPosition }, 40);
		}
		return newBiome;
	}

	public newChunkRight(): void {
		const id: number = this.rightId + 1;
		this.rightId = id;
		if (id < this.nextRightBiomePos) {
			this.map.push(new Chunk(this, id, this.map[this.map.findIndex((el) => el.id === id - 1)].biome));
		} else {
			const newBiome = this.chooseBiomeRight(id);
			if (newBiome.scope.to - newBiome.scope.from < newBiome.minWidth) {
				this.map.push(new Chunk(this, id, this.map[this.map.findIndex((el) => el.id === id - 1)].biome));
			} else {
				this.map.push(new Chunk(this, id, newBiome));
			}
		}
	}

	public newChunkLeft(): void {
		const id: number = this.leftId - 1;
		this.leftId = id;
		if (id > this.nextLeftBiomePos) {
			this.map.unshift(new Chunk(this, id, this.map[this.map.findIndex((el) => el.id === id + 1)].biome));
		} else {
			const newBiome = this.chooseBiomeLeft(id);
			if (Math.abs(newBiome.scope.to - newBiome.scope.from) < newBiome.minWidth) {
				this.map.unshift(new Chunk(this, id, this.map[this.map.findIndex((el) => el.id === id + 1)].biome));
			} else {
				this.map.unshift(new Chunk(this, id, newBiome));
			}
		}
		this.spawn += 1;
	}

	public addUpdate(block: Block): void {
		this.nextUpdate.push(block);
	}

	public goToNextUpdate(): void {
		this.needsUpdate = this.nextUpdate;
		this.nextUpdate = [];
	}

	public getSize(): Size {
		return this.size;
	}

	public getCamera(): Camera {
		return this.camera;
	}

	public getSpawn(): number {
		return this.spawn;
	}

	public addEntity(idChunk: number, entity: Entity): void {
		this.entities.push(entity);
		this.map[idChunk].addEntity(entity);
	}

	// temporaire place the player at spawn height
	public spawnPlayer(player: Player): void {
		//player.setPos({x: 0, y: -this.map[this.spawn].foreground.length*this.size});

		player.setMiddlePos({
			x: 0.5,
			y: this.map[this.spawn].getForegroundHeight(),
		});
		this.addEntity(0, player);
		this.centerCamera(player);

		//player.setChunk
	}

	public centerCamera(player?: Player): void {
		if (player != undefined) {
			this.camera.pos = {
				x: Math.floor(player.position.x * this.size.w - window.innerWidth / 2),
				y: Math.floor(-player.position.y * this.size.h - window.innerHeight / 2),
			};
		} else {
			this.camera.pos = {
				x: Math.floor(-window.innerWidth / 2),
				y: Math.floor(-this.getChunk(0).getForegroundHeight() * this.size.h - window.innerHeight / 2),
			};
		}
	}

	public getChunk(id: number): Chunk {
		return this.map.find((el) => el.id === id);
	}

	public getChunkById(id: number): Chunk {
		return this.map[id + this.spawn];
	}

	public render(backgroundCanvas: Canvas, foregroundCanvas: Canvas, entitiesCanvas: Canvas): void {
		// Log: cam pos vs loaded chunks
		//console.log(this.cam.getPos().x + " " + this.cam.getPos().y + this.cam.getSize().x);
		//console.log(Math.floor(this.cam.getPos().x)/10 + " " + Math.floor((this.cam.getPos().y + this.cam.getSize().x)/10));
		//console.log(this.map[0].id + " " + this.map[this.map.length-1].id);

		// Charger nouveaux chunks en fonction de la cam

		// render all
		/*this.map.forEach((c) => {
			c.render(canvas, this.cam, this.size);
		});*/

		// render only what's visible by cam
		const rendered = new Set<Entity>();
		for (let i: number = Math.floor(this.camera.pos.x / this.size.w); i <= Math.floor((this.camera.pos.x + this.camera.size.w) / this.size.w); i++) {
			this.map[i + this.spawn].render(backgroundCanvas, foregroundCanvas, this.camera, this.size);
			this.map[i + this.spawn].renderEntities(entitiesCanvas, this.camera, this.size, rendered);
		}

		/* not needed anymore because of double canvas
		for (let i: number = Math.floor(this.camera.pos.x / this.size.x); i <= Math.floor((this.camera.pos.x + this.camera.size.x) / this.size.x); i++) {
			this.map[i + this.spawn].renderEntities(canvas, this.camera, this.size);
		}*/

		//player.render(canvas, this.cam);
	}

	/*public moveEntity(chunk: Chunk, entity: Entity): void {
		entity.chunk.removeEntity(entity);
		chunk.addEntity(entity);
	}*/

	public setBlock(block: Block): void {
		//console.log(position);
		if (block.position.x + this.spawn >= 0 && block.position.x + this.spawn < this.map.length) {
			this.map[block.position.x + this.spawn].setBlock(block);
		} else {
			// error, chunks not generated
		}
	}

	public removeBlock(position: Position): void {
		//console.log(position);
		if (position.x + this.spawn >= 0 && position.x + this.spawn < this.map.length) {
			this.map[position.x + this.spawn].removeBlock(position.y);

			this.updateBlock({ x: position.x - 1, y: position.y });
			this.updateBlock({ x: position.x, y: position.y + 1 });
			this.updateBlock({ x: position.x, y: position.y - 1 });
			this.updateBlock({ x: position.x + 1, y: position.y });
		} else {
			// error: chunks not generated
		}
	}

	public getBlock(position: Position): Block {
		if (position.x + this.spawn >= 0 && position.x + this.spawn < this.map.length) {
			return this.map[position.x + this.spawn].getBlock(position.y);
		} else {
			//alert("undefined");
			// Todo manage outside of the map search
			return new Air(position, undefined);
		}
	}

	public updateBlock(position: Position): void {
		if (position.x + this.spawn >= 0 && position.x + this.spawn < this.map.length) {
			this.map[position.x + this.spawn].updateBlock(position);
		} else {
			// chunks not generated
		}
	}

	public update(): void {
		this.goToNextUpdate();
		/*this.entities.forEach((entity) => {
			// if the entity isn't in its chunk
			if (Math.floor(entity.getMiddlePos().x) != entity.chunk.id) {
				//console.log("entity is changing chunk");

				// if the entity is in generated chunks
				if (
					Math.floor(entity.getMiddlePos().x / this.size.w) + this.spawn >= 0 &&
					Math.floor(entity.getMiddlePos().x / this.size.w) + this.spawn < this.map.length
				) {
					// if the player is going into generated chunk
					//const isPlayer = (element) => element === player;
					//this.map[player.idChunk + this.spawn].entities.splice(
					//	this.map[player.idChunk + this.spawn].entities.findIndex(isPlayer),
					//	1
					//);
					//this.removeEntity(this.map[entity.chunk.id + this.spawn], entity);
					this.moveEntity(this.map[Math.floor(entity.getMiddlePos().x) + this.spawn], entity);
				} else {
					console.log("%c Error: entity going into ungenerated chunks", "background: red; color: white; font-weight: bold;");
				}
			}
		});*/

		// load new chunks
		//console.log(
		//	"RIGHT : " + right + " ; END ARRAY ID : " + this.map[this.map.length - 1].id + " ; LEFT : " + left + " ; BEGIN ARRAY ID : " + this.map[0].id
		//);
		while (Math.floor((this.camera.pos.x + this.camera.size.w) / this.size.w) /*-1*/ > this.map[this.map.length - 1].id) {
			this.newChunkRight();
		}
		while (Math.floor(this.camera.pos.x / this.size.w) /*+1*/ < this.map[0].id) {
			this.newChunkLeft();
		}

		// update blocks
		// Todo: new update system
		/*for (let i: number = Math.floor(this.camera.pos.x / this.size.x); i <= Math.floor((this.camera.pos.x + this.camera.size.x) / this.size.x); i++) {
			this.map[i + this.spawn].update(this.camera, this.size);
		}*/
		if (this.needsUpdate.length > 0) {
			//console.log("%c TEST: updated blocks", "background: blue; color: white; font-weight: bold;");
		}
		this.needsUpdate.forEach((block) => {
			block.update();
			//console.log(block.name + " " + block.position.x + " " + block.position.y);
		});

		this.map.forEach((chunk) => {
			chunk.randomUpdate();
			//console.log(block.name + " " + block.position.x + " " + block.position.y);
		});

		const updated = new Set<Entity>();
		for (let i: number = Math.floor(this.camera.pos.x / this.size.w); i <= Math.floor((this.camera.pos.x + this.camera.size.w) / this.size.w); i++) {
			this.map[i + this.spawn].updateEntities(updated);
		}
	}
}
