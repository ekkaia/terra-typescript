// type ou interface
export interface Position {
	x: number;
	y: number;
}

export interface Velocity {
	x: number;
	y: number;
}

export interface Acceleration {
	x: number;
	y: number;
}

export interface Size {
	w: number;
	h: number;
}

// Range already used in typescript
export interface Scope {
	from: number;
	to: number;
}

export interface Color {
	r: number;
	g: number;
	b: number;
	a: number;
}

export class Calculation {
	static includes(scope: Scope, number: number): boolean {
		return (number >= scope.from && number <= scope.to) || (number >= scope.to && number <= scope.from);
	}
}
