import { Camera } from "./Camera";
import { Canvas } from "./Canvas";
import { Position, Size } from "./Data";

// Thanks to https://github.com/bmoren from https://github.com/bmoren/p5.collide2D, modified version for typescript
// Todo: Warning, Triangle, Arc and Ellipse should't be used, doesn't collide with most other shapes !

export abstract class Shape {
	private static _collideDebug;
	boolean = false;
	constructor() {}

	// Todo: good method ?
	public abstract relative(pos: Position): Shape;
	public abstract draw(canvas: Canvas, pos: Position, cam: Camera, size: Size): void;
	public abstract toString(): string;

	public static dist(pos1: Position, pos2: Position): number {
		return Math.sqrt(Math.pow(pos1.x - pos2.x, 2) + Math.pow(pos1.y - pos2.y, 2));
	}

	public static collide(shape1: Shape, shape2: Shape): any {
		return this["collide" + shape1.constructor.name + shape2.constructor.name](shape1, shape2);
	}

	public static collideRectangleRectangle(rect1: Rectangle, rect2: Rectangle) {
		//2d
		//add in a thing to detect rectMode CENTER
		if (
			rect1.pos.x + rect1.size.w >= rect2.pos.x && // r1 right edge past r2 left
			rect1.pos.x <= rect2.pos.x + rect2.size.w && // r1 left edge past r2 right
			rect1.pos.y + rect1.size.h >= rect2.pos.y && // r1 top edge past r2 bottom
			rect1.pos.y <= rect2.pos.y + rect2.size.h
		) {
			// r1 bottom edge past r2 top
			return true;
		}
		//! Modified it !
		return false;
	}

	public static collideCircleRectangle(circle: Circle, rectangle: Rectangle) {
		return this.collideRectangleCircle(rectangle, circle);
	}

	public static collideRectangleCircle(rectangle: Rectangle, circle: Circle) {
		//2d
		// temporary variables to set edges for testing
		let testX = circle.pos.x;
		let testY = circle.pos.y;

		// which edge is closest?
		if (circle.pos.x < rectangle.pos.x) {
			// left edge
			testX = rectangle.pos.x;
		} else if (circle.pos.x > rectangle.pos.x + rectangle.size.w) {
			// right edge
			testX = rectangle.pos.x + rectangle.size.w;
		}

		if (circle.pos.y < rectangle.pos.y) {
			// top edge
			testY = rectangle.pos.y;
		} else if (circle.pos.y > rectangle.pos.y + rectangle.size.h) {
			// bottom edge
			testY = rectangle.pos.y + rectangle.size.h;
		}

		// // get distance from closest edges
		const distance = this.dist(circle.pos, { x: testX, y: testY });

		// if the distance is less than the radius, collision!
		if (distance <= circle.diameter / 2) {
			return true;
		}
		return false;
	}

	public static collideCircleCircle(c1: Circle, c2: Circle) {
		//2d
		if (this.dist({ x: c1.pos.x, y: c1.pos.y }, { x: c2.pos.x, y: c2.pos.y }) <= c1.diameter / 2 + c2.diameter / 2) {
			return true;
		}
		return false;
	}

	public static collideCirclePoint(circle: Circle, point: Point) {
		return this.collidePointCircle(point, circle);
	}

	public static collidePointCircle(point: Point, circle: Circle) {
		//2d
		if (this.dist({ x: point.x, y: point.y }, { x: circle.pos.x, y: circle.pos.y }) <= circle.diameter / 2) {
			return true;
		}
		return false;
	}

	public static collideEllipsePoint(ellipse: Ellipse, point: Point) {
		return this.collidePointEllipse(point, ellipse);
	}

	public static collidePointEllipse(point: Point, ellipse: Ellipse) {
		//2d
		const rx = ellipse.diameter.w / 2,
			ry = ellipse.diameter.h / 2;
		// Discarding the points outside the bounding box
		if (point.x > ellipse.pos.x + rx || point.x < ellipse.pos.x - rx || point.y > ellipse.pos.y + ry || point.y < ellipse.pos.y - ry) {
			return false;
		}
		// Compare the point to its equivalent on the ellipse
		const xx = point.x - ellipse.pos.x,
			yy = point.y - ellipse.pos.y;
		const eyy = (ry * Math.sqrt(Math.abs(rx * rx - xx * xx))) / rx;
		return yy <= eyy && yy >= -eyy;
	}

	public static collideRectanglePoint(rectangle: Rectangle, point: Point) {
		return this.collidePointRectangle(point, rectangle);
	}

	public static collidePointRectangle(point: Point, rectangle: Rectangle) {
		//2d
		if (
			point.x >= rectangle.pos.x && // right of the left edge AND
			point.x <= rectangle.pos.x + rectangle.size.w && // left of the right edge AND
			point.y >= rectangle.pos.y && // below the top AND
			point.y <= rectangle.pos.y + rectangle.size.h
		) {
			// above the bottom
			return true;
		}
		return false;
	}

	public static collideSegmentPoint(segment: Segment, point: Point) {
		return this.collidePointSegment(point, segment);
	}

	public static collidePointSegment(point: Point, segment: Segment) {
		// Todo: Temporary buffer ???
		let buffer;

		// get distance from the point to the two ends of the line
		const d1 = this.dist({ x: point.x, y: point.y }, { x: segment.from.x, y: segment.from.y });
		const d2 = this.dist({ x: point.x, y: point.y }, { x: segment.to.x, y: segment.to.y });

		// get the length of the line
		const lineLen = this.dist({ x: segment.from.x, y: segment.from.y }, { x: segment.to.x, y: segment.to.y });

		// since floats are so minutely accurate, add a little buffer zone that will give collision
		if (buffer === undefined) {
			buffer = 0.1;
		} // higher # = less accurate

		// if the two distances are equal to the line's length, the point is on the line!
		// note we use the buffer here to give a range, rather than one #
		if (d1 + d2 >= lineLen - buffer && d1 + d2 <= lineLen + buffer) {
			return true;
		}
		return false;
	}

	public static collideCircleSegment(circle: Circle, segment: Segment) {
		return this.collideSegmentCircle(segment, circle);
	}

	public static collideSegmentCircle(segment: Segment, circle: Circle) {
		// is either end INSIDE the circle?
		// if so, return true immediately
		const inside1 = this.collidePointCircle(segment.from, circle);
		const inside2 = this.collidePointCircle(segment.to, circle);
		if (inside1 || inside2) return true;

		// get length of the line
		let distX = segment.from.x - segment.to.x;
		let distY = segment.from.y - segment.to.y;
		const len = Math.sqrt(distX * distX + distY * distY);

		// get dot product of the line and circle
		const dot =
			((circle.pos.x - segment.from.x) * (segment.to.x - segment.from.x) + (circle.pos.y - segment.from.y) * (segment.to.y - segment.from.y)) /
			Math.pow(len, 2);

		// find the closest point on the line
		const closestX = segment.from.x + dot * (segment.to.x - segment.from.x);
		const closestY = segment.from.y + dot * (segment.to.y - segment.from.y);

		// is this point actually on the line segment?
		// if so keep going, but if not, return false
		const onSegment = this.collidePointSegment(new Point({ x: closestX, y: closestY }), segment);
		if (!onSegment) return false;

		// draw a debug circle at the closest point on the line
		if (this._collideDebug) {
			// Todo: What is this
			//this.ellipse(closestX, closestY,10,10);
		}

		// get distance to closest point
		distX = closestX - circle.pos.x;
		distY = closestY - circle.pos.y;
		const distance = Math.sqrt(distX * distX + distY * distY);

		if (distance <= circle.diameter / 2) {
			return true;
		}
		return false;
	}

	public static collideSegmentSegment(seg1: Segment, seg2: Segment, calcIntersection = false): boolean | Position {
		let intersection;

		// calculate the distance to intersection point
		const uA =
			((seg2.to.x - seg2.from.x) * (seg1.from.y - seg2.from.y) - (seg2.to.y - seg2.from.y) * (seg1.from.x - seg2.from.x)) /
			((seg2.to.y - seg2.from.y) * (seg1.to.x - seg1.from.x) - (seg2.to.x - seg2.from.x) * (seg1.to.y - seg1.from.y));
		const uB =
			((seg1.to.x - seg1.from.x) * (seg1.from.y - seg2.from.y) - (seg1.to.y - seg1.from.y) * (seg1.from.x - seg2.from.x)) /
			((seg2.to.y - seg2.from.y) * (seg1.to.x - seg1.from.x) - (seg2.to.x - seg2.from.x) * (seg1.to.y - seg1.from.y));

		// if uA and uB are between 0-1, lines are colliding
		if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
			let intersectionX: number;
			let intersectionY: number;
			if (this._collideDebug || calcIntersection) {
				// calc the point where the lines meet
				intersectionX = seg1.from.x + uA * (seg1.to.x - seg1.from.x);
				intersectionY = seg1.from.y + uA * (seg1.to.y - seg1.from.y);
			}

			if (this._collideDebug) {
				// Todo: what is this ?
				//this.ellipse(intersectionX,intersectionY,10,10);
			}

			if (calcIntersection) {
				intersection = {
					x: intersectionX,
					y: intersectionY,
				};
				return intersection;
			} else {
				return true;
			}
		}
		if (calcIntersection) {
			intersection = {
				x: false,
				y: false,
			};
			return intersection;
		}
		return false;
	}

	public static collideRectangleSegment(rectangle: Rectangle, segment: Segment) {
		return this.collideSegmentRectangle(segment, rectangle);
	}

	public static collideSegmentRectangle(segment: Segment, rectangle: Rectangle, calcIntersection = false): any {
		// check if the line has hit any of the rectangle's sides. uses the collideLineLine function above
		let left, right, top, bottom, intersection;

		if (calcIntersection) {
			left = this.collideSegmentSegment(segment, rectangle.getLeftSegment(), true);
			right = this.collideSegmentSegment(segment, rectangle.getRightSegment(), true);
			top = this.collideSegmentSegment(segment, rectangle.getTopSegment(), true);
			bottom = this.collideSegmentSegment(segment, rectangle.getBottomSegment(), true);
			intersection = {
				left: left,
				right: right,
				top: top,
				bottom: bottom,
			};
		} else {
			//return booleans
			left = this.collideSegmentSegment(segment, rectangle.getLeftSegment());
			right = this.collideSegmentSegment(segment, rectangle.getRightSegment());
			top = this.collideSegmentSegment(segment, rectangle.getTopSegment());
			bottom = this.collideSegmentSegment(segment, rectangle.getBottomSegment());
		}

		// if ANY of the above are true, the line has hit the rectangle
		if (left || right || top || bottom) {
			if (calcIntersection) {
				return intersection;
			}
			return true;
		}
		return false;
	}

	public static collidePolygonPoint(polygon: Polygon, point: Point) {
		return this.collidePointPolygon(point, polygon);
	}

	public static collidePointPolygon(point: Point, polygon: Polygon): boolean {
		let collision = false;

		// go through each of the vertices, plus the next vertex in the list
		let next = 0;
		for (let current = 0; current < polygon.pos.length; current++) {
			// get next vertex in list if we've hit the end, wrap around to 0
			next = current + 1;
			if (next === polygon.pos.length) next = 0;

			// get the PVectors at our current position this makes our if statement a little cleaner
			const vc = polygon.pos[current]; // c for "current"
			const vn = polygon.pos[next]; // n for "next"

			// compare position, flip 'collision' variable back and forth
			if (
				((vc.y >= point.y && vn.y < point.y) || (vc.y < point.y && vn.y >= point.y)) &&
				point.x < ((vn.x - vc.x) * (point.y - vc.y)) / (vn.y - vc.y) + vc.x
			) {
				collision = !collision;
			}
		}
		return collision;
	}

	public static collidePolygonCircle(polygon: Polygon, circle: Circle) {
		return this.collideCirclePolygon(circle, polygon);
	}

	public static collideCirclePolygon(circle: Circle, polygon: Polygon, interior = false) {
		// go through each of the vertices, plus the next vertex in the list
		let next = 0;
		for (let current = 0; current < polygon.pos.length; current++) {
			// get next vertex in list if we've hit the end, wrap around to 0
			next = current + 1;
			if (next === polygon.pos.length) next = 0;

			// get the PVectors at our current position this makes our if statement a little cleaner
			const vc = polygon.pos[current]; // c for "current"
			const vn = polygon.pos[next]; // n for "next"

			// check for collision between the circle and a line formed between the two vertices
			const collision = this.collideSegmentCircle(new Segment({ x: vc.x, y: vc.y }, { x: vn.x, y: vn.y }), circle);
			if (collision) return true;
		}

		// test if the center of the circle is inside the polygon
		if (interior === true) {
			const centerInside = this.collidePointPolygon(new Point(circle.pos), polygon);
			if (centerInside) return true;
		}

		// otherwise, after all that, return false
		return false;
	}

	public static collidePolygonRectangle(polygon: Polygon, rectangle: Rectangle) {
		return this.collideRectanglePolygon(rectangle, polygon);
	}

	public static collideRectanglePolygon(rectangle: Rectangle, polygon: Polygon, interior = false) {
		// go through each of the vertices, plus the next vertex in the list
		let next = 0;
		for (let current = 0; current < polygon.pos.length; current++) {
			// get next vertex in list if we've hit the end, wrap around to 0
			next = current + 1;
			if (next === polygon.pos.length) next = 0;

			// get the PVectors at our current position this makes our if statement a little cleaner
			const vc = polygon.pos[current]; // c for "current"
			const vn = polygon.pos[next]; // n for "next"

			// check against all four sides of the rectangle
			const collision = this.collideSegmentRectangle(new Segment({ x: vc.x, y: vc.y }, { x: vn.x, y: vn.y }), rectangle);
			if (collision) return true;

			// optional: test if the rectangle is INSIDE the polygon note that this iterates all sides of the polygon again, so only use this if you need to
			if (interior === true) {
				const inside = this.collidePointPolygon(new Point(rectangle.pos), polygon);
				if (inside) return true;
			}
		}

		return false;
	}

	public static collidePolygonSegment(polygon: Polygon, segment: Segment) {
		return this.collideSegmentPolygon(segment, polygon);
	}

	public static collideSegmentPolygon(segment: Segment, polygon: Polygon): any {
		// go through each of the vertices, plus the next vertex in the list
		let next = 0;
		for (let current = 0; current < polygon.pos.length; current++) {
			// get next vertex in list if we've hit the end, wrap around to 0
			next = current + 1;
			if (next === polygon.pos.length) next = 0;

			// get the PVectors at our current position extract X/Y coordinates from each
			const x3 = polygon.pos[current].x;
			const y3 = polygon.pos[current].y;
			const x4 = polygon.pos[next].x;
			const y4 = polygon.pos[next].y;

			// do a Line/Line comparison if true, return 'true' immediately and stop testing (faster)
			const hit = this.collide(segment, new Segment({ x: x3, y: y3 }, { x: x4, y: y4 }));
			if (hit) {
				return true;
			}
		}
		// never got a hit
		return false;
	}

	public static collidePolygonPolygon(p1: Polygon, p2: Polygon, interior = false) {
		// go through each of the vertices, plus the next vertex in the list
		let next = 0;
		for (let current = 0; current < p1.pos.length; current++) {
			// get next vertex in list, if we've hit the end, wrap around to 0
			next = current + 1;
			if (next === p1.pos.length) next = 0;

			// get the PVectors at our current position this makes our if statement a little cleaner
			const vc = p1[current]; // c for "current"
			const vn = p1[next]; // n for "next"

			//use these two points (a line) to compare to the other polygon's vertices using polyLine()
			let collision = this.collideSegmentPolygon(new Segment({ x: vc.x, y: vc.y }, { x: vn.x, y: vn.y }), p2);
			if (collision) return true;

			//check if the either polygon is INSIDE the other
			if (interior === true) {
				collision = this.collidePointPolygon(new Point({ x: p2[0].x, y: p2[0].y }), p1);
				if (collision) return true;
				collision = this.collidePointPolygon(new Point({ x: p1[0].x, y: p1[0].y }), p2);
				if (collision) return true;
			}
		}

		return false;
	}

	public static collideTrianglePoint(triangle: Triangle, point: Point) {
		return this.collidePointTriangle(point, triangle);
	}

	public static collidePointTriangle(point: Point, triangle: Triangle) {
		// get the area of the triangle
		const areaOrig = Math.abs(
			(triangle.pos2.x - triangle.pos1.x) * (triangle.pos3.y - triangle.pos1.y) -
				(triangle.pos3.x - triangle.pos1.x) * (triangle.pos2.y - triangle.pos1.y)
		);

		// get the area of 3 triangles made between the point and the corners of the triangle
		const area1 = Math.abs((triangle.pos1.x - point.x) * (triangle.pos2.y - point.y) - (triangle.pos2.x - point.x) * (triangle.pos1.y - point.y));
		const area2 = Math.abs((triangle.pos2.x - point.x) * (triangle.pos3.y - point.y) - (triangle.pos3.x - point.x) * (triangle.pos2.y - point.y));
		const area3 = Math.abs((triangle.pos3.x - point.x) * (triangle.pos1.y - point.y) - (triangle.pos1.x - point.x) * (triangle.pos3.y - point.y));

		// if the sum of the three areas equals the original, we're inside the triangle!
		if (area1 + area2 + area3 === areaOrig) {
			return true;
		}
		return false;
	}

	public static collidePointPoint(p1: Point, p2: Point, buffer = 0) {
		if (this.dist(p1, p2) <= buffer) {
			return true;
		}

		return false;
	}

	public static collideArcPoint(arc: Arc, point: Point) {
		return this.collidePointArc(point, arc);
	}

	public static collidePointArc(point: Point, arc: Arc, buffer = 0) {
		// Todo: WHAT THE HELL IS THAT
		const radius = undefined; //this.createVector(arc.radius, 0).rotate(arc.heading);

		const pointToArc = point.copy().sub(arc.pos);

		if (point.dist(arc.pos) <= arc.radius + buffer) {
			const dot = radius.dot(pointToArc);
			const angle = radius.angleBetween(pointToArc);
			if (dot > 0 && angle <= arc.angle / 2 && angle >= -arc.angle / 2) {
				return true;
			}
		}
		return false;
	}
}

export class Segment extends Shape {
	public from: Point;
	public to: Point;
	constructor(from: Position | Point, to: Position | Point) {
		super();

		if (from instanceof Point) {
			this.from = from;
		} else {
			this.from = new Point(from);
		}

		if (to instanceof Point) {
			this.to = to;
		} else {
			this.to = new Point(to);
		}
	}

	public draw(canvas: Canvas): void {
		canvas.line({
			from: { x: this.from.x, y: this.from.y },
			to: { x: this.to.x, y: this.to.y },
		});
	}

	public relative(pos: Position): Segment {
		return new Segment({ x: this.from.x + pos.x, y: this.from.y + pos.y }, { x: this.to.x + pos.x, y: this.to.y + pos.y });
	}

	public toString(): string {
		return "Segment : " + this.from.toString + this.to.toString();
	}

	public collideWith(shape: Shape) {
		return Shape.collide(this, shape);
	}
}

export class Point extends Shape {
	public x: number;
	public y: number;
	constructor(pos: Position) {
		super();
		(this.x = pos.x), (this.y = pos.y);
	}

	public draw(canvas: Canvas, pos: Position, cam: Camera, size: Size): void {
		canvas.line({
			from: { x: (this.x + pos.x) * size.w - cam.pos.x, y: -(this.y + pos.y - 1) * size.h - cam.pos.y },
			to: { x: (this.x + pos.x) * size.w - cam.pos.x, y: -(this.y + pos.y - 1) * size.h - cam.pos.y },
		});
	}

	public relative(pos: Position): Point {
		return new Point({ x: this.x + pos.x, y: this.y + pos.y });
	}

	public toString(): string {
		return "Point : " + this.x + " " + this.y;
	}

	public collideWith(shape: Shape) {
		return Shape.collide(this, shape);
	}

	public dist(pos: Position): number {
		return Shape.dist(this, pos);
	}

	public copy(): Point {
		return new Point({ x: this.x, y: this.y });
	}

	public sub(pos: Position) {
		this.x -= pos.x;
		this.y -= pos.y;
	}
}

export class Circle extends Shape {
	public pos: Position;
	public diameter: number;
	constructor(pos: Position, diameter: number) {
		super();
		this.pos = pos;
		this.diameter = diameter;
	}

	public draw(canvas: Canvas, pos: Position, cam: Camera, size: Size): void {
		canvas.circle({ x: (this.pos.x + pos.x) * size.w - cam.pos.x, y: -(this.pos.y + pos.y - 1) * size.h - cam.pos.y }, this.diameter);
	}

	public relative(pos: Position): Circle {
		return new Circle({ x: this.pos.x + pos.x, y: this.pos.y + pos.y }, this.diameter);
	}

	public toString(): string {
		return "Circle : " + this.pos.x, +" " + this.pos.y + "";
	}

	public collideWith(shape: Shape) {
		return Shape.collide(this, shape);
	}
}

export class Rectangle extends Shape {
	public pos: Position;
	public size: Size;
	constructor(pos: Position, size: Size) {
		super();
		this.pos = pos;
		this.size = size;
	}

	public draw(canvas: Canvas, pos: Position, cam: Camera, size: Size): void {
		canvas.rect(
			{ x: (this.pos.x + pos.x) * size.w - cam.pos.x, y: -(this.pos.y + pos.y + this.size.h - 1) * size.h - cam.pos.y },
			{ w: this.size.w * size.w, h: this.size.h * size.h }
		);
	}

	public relative(pos: Position): Rectangle {
		return new Rectangle({ x: this.pos.x + pos.x, y: this.pos.y + pos.y }, { w: this.size.w, h: this.size.h });
	}

	public toString(): string {
		return "Rectangle : " + this.pos.x + " " + this.pos.y + " " + this.size.w + " " + this.size.h;
	}

	public collideWith(shape: Shape) {
		return Shape.collide(this, shape);
	}

	public getBottomSegment(): Segment {
		return new Segment({ x: this.pos.x, y: this.pos.y }, { x: this.pos.x + this.size.w, y: this.pos.y });
	}

	public getRightSegment(): Segment {
		return new Segment({ x: this.pos.x + this.size.w, y: this.pos.y }, { x: this.pos.x + this.size.w, y: this.pos.y + this.size.h });
	}

	public getLeftSegment(): Segment {
		return new Segment({ x: this.pos.x, y: this.pos.y }, { x: this.pos.x, y: this.pos.y + this.size.h });
	}

	public getTopSegment(): Segment {
		return new Segment({ x: this.pos.x, y: this.pos.y + this.size.h }, { x: this.pos.x + this.size.w, y: this.pos.y + this.size.h });
	}
}

// Todo: Shouldn't be used
export class Triangle extends Shape {
	public pos1: Position;
	public pos2: Position;
	public pos3: Position;
	constructor(pos1: Position, pos2: Position, pos3: Position) {
		super();
		this.pos1 = pos1;
		this.pos2 = pos2;
		this.pos3 = pos3;
	}

	public draw(canvas: Canvas): void {
		// TODO
	}

	public relative(pos: Position): Triangle {
		return new Triangle(
			{ x: this.pos1.x + pos.x, y: -this.pos1.y + pos.y },
			{ x: this.pos2.x + pos.x, y: this.pos2.y + pos.y },
			{ x: this.pos3.x + pos.x, y: this.pos3.y + pos.y }
		);
	}

	public toString(): string {
		return "Triangle : " + this.pos1.x + " " + this.pos1.y + " " + this.pos2.x + " " + this.pos2.y + " " + this.pos3.x + " " + this.pos3.y;
	}
}

export class Polygon extends Shape {
	public pos: Point[];

	constructor(pos: Point[]) {
		super();
		this.pos = pos;
	}

	public draw(canvas: Canvas, pos: Position, cam: Camera, size: Size): void {
		let next = 0;
		for (let current = 0; current < this.pos.length; current++) {
			// get next vertex in list if we've hit the end, wrap around to 0
			next = current + 1;
			if (next === this.pos.length) next = 0;

			canvas.line(
				new Segment(
					{ x: (this.pos[current].x + pos.x) * size.w - cam.pos.x, y: -(this.pos[current].y + pos.y - 1) * size.h - cam.pos.y },
					{ x: (this.pos[next].x + pos.x) * size.w - cam.pos.x, y: -(this.pos[next].y + pos.y - 1) * size.h - cam.pos.y }
				)
			);
		}
	}

	public relative(pos: Position): Polygon {
		const out: Point[] = [];
		this.pos.forEach((point) => {
			out.push(new Point({ x: point.x + pos.x, y: point.y + pos.y }));
		});
		return new Polygon(out);
	}

	public toString(): string {
		let out = "Polygon : ";
		this.pos.forEach((point) => {
			out += point.toString();
		});
		return out;
	}

	public collideWith(shape: Shape) {
		return Shape.collide(this, shape);
	}
}

// Todo: Shouldn't be used
export class Arc extends Shape {
	public pos: Position;
	public radius: number;
	public heading: number;
	public angle: number;
	constructor(pos: Position, radius: number, heading: number, angle: number) {
		super();
		this.pos = pos;
		this.radius = radius;
		this.heading = heading;
		this.angle = angle;
	}

	public draw(canvas: Canvas): void {
		// TODO
	}

	public relative(pos: Position): Arc {
		return new Arc({ x: this.pos.x + pos.x, y: this.pos.y + pos.y }, this.radius, this.heading, this.angle);
	}

	public toString(): string {
		return "Arc : ";
	}
}

// Todo: Shouldn't be used
export class Ellipse extends Shape {
	public pos: Position;
	public diameter: Size;
	constructor(pos: Position, diameter: Size) {
		super();
		this.pos = pos;
		this.diameter = diameter;
	}

	public draw(canvas: Canvas): void {
		// TODO
	}

	public relative(pos: Position): Ellipse {
		return new Ellipse({ x: this.pos.x + pos.x, y: this.pos.y + pos.y }, this.diameter);
	}

	public toString(): string {
		return "Ellipse : ";
	}
}
