import { input } from "./app";
import { Block } from "./blocks/Block";
import { Air } from "./blocks/Air";
import { Dirt } from "./blocks/Dirt";
import { Grass } from "./blocks/Grass";
import { Stone } from "./blocks/Stone";
import { Water } from "./blocks/Water";
import { Camera } from "./Camera";
import { Position, Size } from "./Data";
import { Liquid } from "./Liquid";
import { World } from "./World";
import { Stair } from "./blocks/Stair";

export class Input {
	private world;
	/* +++++ VARIABLES ENTREES CLAVIER SOURIS +++++ */
	public right = false;
	public left = false;

	public up = false;
	public down = false;

	public ctrl = false;
	public shift = false;

	public leftPressed = false;
	public rightPressed = false;

	public mouse: Position;
	public select: Position;

	public selected = 0;

	/* ----- FIN VARIABLES ENTREES CLAVIER SOURIS ----- */

	constructor(world: World) {
		this.world = world;
		this.mouse = { x: 0, y: 100 };
		this.select = { x: 0, y: 0 };

		this.events();
		this.test();
	}

	private move(e: { pageX: number; pageY: number }): void {
		this.mouse = { x: e.pageX, y: e.pageY };
	}

	// Get the pixel position of the block you point at
	public getMouseBlockPos(camera: Camera = this.world.dimension[this.world.active].camera, size: Size = this.world.dimension[this.world.active].size): Position {
		return {
			x: Math.floor((this.mouse.x + camera.pos.x) / size.w) * size.w - camera.pos.x,
			y: Math.floor((this.mouse.y + camera.pos.y) / size.h) * size.h - camera.pos.y,
		};
	}

	// get the block position you point at
	public getMouseBlock(camera: Camera = this.world.dimension[this.world.active].camera, size: Size = this.world.dimension[this.world.active].size) {
		return {
			x: Math.floor((this.mouse.x + camera.pos.x) / size.w),
			y: -Math.floor((this.mouse.y + camera.pos.y) / size.h),
		};
	}


	public getMousePositionInsideBlock(camera: Camera = this.world.dimension[this.world.active].camera, size: Size = this.world.dimension[this.world.active].size) {
		let blockPos = this.getMouseBlockPos(camera, size);
		return {
			x: (this.mouse.x - blockPos.x) / size.w,
			y: 1 - (this.mouse.y - blockPos.y) / size.h,
		};
	}

	public events(): void {
		document.addEventListener("mousedown", this.click);
		document.addEventListener("mouseup", this.unclick);
		document.addEventListener("mousemove", (e) => {
			this.mouse = { x: e.pageX, y: e.pageY };
		});
		document.addEventListener("contextmenu", (e) => {
			e.preventDefault();
			this.unclick(e);
		});
	}

	public update(): void {
		//console.log(this.mouse);
	}

	private click(e): void {
		if (e.button == 0) {
			//console.log("clic");
			this.leftPressed = true;
			input.leftClicked();
			//console.log("end clic");
			/*this.clickButton();
			if(menu.actuel == "jeux"){
				this.selection();
			}*/
		} else if (e.button == 2) {
			this.rightPressed = true;
			input.rightClicked();
			/*if(menu.actuel == "jeux"){
			this.poseSelection();
			}*/
		}
	}

	public leftClicked(): void {
		this.world.leftClicked();
	}

	public rightClicked(): void {
		/*if (this.world.player.getBlock() instanceof Air) {
			this.world.player.placeBlock(new Water(new Liquid()));
		} else if (this.world.player.getBlock() instanceof Water) {
			if (this.world.player.getBlock().liquid.volume + 100 <= this.world.player.getBlock().liquid.quantity * 100) {
				this.world.player.getBlock().liquid.volume += 100;
			}
			else {
				// Todo: place when what you place is bellow 100 ?
				this.world.player.getBlock().liquid.volume = this.world.player.getBlock().liquid.quantity * 100;
			}
		}*/
		let placing: any;
		switch (this.selected) {
			// Todo: NEEDS BETTER SOLUTION
			case 0:
				placing = Dirt; //new Dirt(undefined, this.world.dimension[this.world.active].map[0]);
				break;
			case 1:
				placing = Grass; //new Grass(undefined, undefined);
				break;
			case 2:
				placing = Stone; //new Stone(undefined, this.world.dimension[this.world.active].map[0]);
				break;
			case 3:
				placing = Water; //new Water(undefined, this.world.dimension[this.world.active].map[0], new Liquid());
				break;
			case 4:
				placing = Stair; //new Stair(undefined, undefined);
				break;
			default:
				placing = Air; //new Air(undefined, undefined);
				break;
		}
		this.world.rightClicked(placing);
	}

	public middleClicked(): void {
		console.log("Middle clicked");
	}

	private unclick(e): void {
		if (e.button == 0) {
			this.leftPressed = false;
			/*if(menu.actuel == "jeux"){
			this.nonselection();
			}*/
		} else if (e.button == 2) {
			this.rightPressed = false;
		}
	}

	test(): void {
		// touches enfoncées
		document.onkeydown = (event) => {
			switch (event.keyCode) {
				case 13: // enter entrer
					this.world.minimapTest = !this.world.minimapTest;
					break;
				case 81: // q
					this.left = true;
					//localWorld[currentWorld].player.facing = "ouest";
					break;

				case 90: // z
					/*try {
					if ((typeof localWorld[currentWorld].player.deplacement == "number" && nombreSaut > 0) || localWorld[currentWorld].player.deplacement == "infini" || (localWorld[currentWorld].player.deplacement != "non" && (localWorld[currentWorld].dim[currentDim].map[Math.floor((localWorld[currentWorld].player.posY + localWorld[currentWorld].player.taille + 1) / cube)][Math.floor((localWorld[currentWorld].player.posX + localWorld[currentWorld].player.taille) / cube)] != " " || localWorld[currentWorld].dim[currentDim].map[Math.floor((localWorld[currentWorld].player.posY + localWorld[currentWorld].player.taille + 1) / cube)][Math.floor(localWorld[currentWorld].player.posX / cube)] != " "))) {
						localWorld[currentWorld].player.accelerationY = localWorld[currentWorld].player.jump;
						if (typeof localWorld[currentWorld].player.deplacement == "number" && localWorld[currentWorld].player.deplacement > 0) {
						nombreSaut--;
						}
					}
					} catch (err) {}*/
					this.up = true;
					break;
				case 32: // space
					this.world.test();
					break;
				case 68: // d
					this.right = true;
					//localWorld[currentWorld].player.facing = "est";
					break;

				case 83: // s
					this.down = true;
					break;
				case 17: // ctrl
					//ctrl
					this.ctrl = true;
					break;

				case 16: // shift
					this.shift = true;
					break;
				case 70: // f
					this.world.player.fly = !this.world.player.fly;
					break;
				case 78: // n
					this.world.player.noclip = !this.world.player.noclip;
					break;
				case 37:
					// flèche left
					//localWorld[currentWorld].camera.posX--;
					this.world.dimension[this.world.active].getCamera().left(20);
					break;

				case 39:
					// flèche right
					//localWorld[currentWorld].camera.posX++
					this.world.dimension[this.world.active].getCamera().right(20);
					break;

				case 38:
					// flèche up
					/*if (localWorld[currentWorld].camera.posY > 0) {
					localWorld[currentWorld].camera.posY--;
					}*/
					this.world.dimension[this.world.active].getCamera().up(20);
					break;

				case 40:
					// flèche down
					/*if (localWorld[currentWorld].camera.posY < localWorld[currentWorld].dim[currentDim].mapY - gameY / cube) {
					localWorld[currentWorld].camera.posY++;
					}*/
					this.world.dimension[this.world.active].getCamera().down(20);
					break;

				case 109:
					// -
					//loadMap(world, 500, "ouest");
					this.world.dimension[this.world.active].zoom(-1);
					break;

				case 107:
					//+
					//loadMap(world, 500, "est");
					this.world.dimension[this.world.active].zoom(1);
					break;

				case 80:
					// P
					//createProjectile(localWorld[currentWorld].dim[currentDim], Math.floor(localWorld[currentWorld].player.posX+localWorld[currentWorld].player.taille/2), localWorld[currentWorld].player.posY, (mouseX - (localWorld[currentWorld].player.posX - localWorld[currentWorld].camera.posX * cube)) / Pdiviseur, Math.abs(mouseY - (localWorld[currentWorld].player.posY - localWorld[currentWorld].camera.posY * cube)) / Pdiviseur, "Cha", 1, true);
					break;
				case 88:
					// X
					/*if(localWorld[currentWorld].player.currentSlot != "none" && localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o != " "){
					if(ctrl == false){
						drop(localWorld[currentWorld], localWorld[currentWorld].player.currentSlot, 1);
					} else {
						drop(localWorld[currentWorld], localWorld[currentWorld].player.currentSlot, "tout");
					}
					}*/
					break;
				case 73:
					// I
					/*if(menu.actuel == "jeux"){
					if (typeInventaire > 0) {
						typeInventaire = 0;
						bouton.splice(bouton.length-1,1);
						for(var i = 0; i < localWorld[currentWorld].player.slot.length; i++){
						if(localWorld[currentWorld].player.slot[i].drop == true && localWorld[currentWorld].player.slot[i].o != " "){
							drop(localWorld[currentWorld], i,localWorld[currentWorld].player.slot[i].q);
							localWorld[currentWorld].player.slot[i].o = " ";
							localWorld[currentWorld].player.slot[i].q = 0;
						}
						}
						//son.player.fermeInventaire.play()
					} else {
						typeInventaire = 1;
						boutons(730, gameY - 70 * 3 + 135, 50, 50, "crafting(localWorld[currentWorld]);", texture.inventaire.tan, texture.inventaire.brown, objet["TdC"].texture, 0.75);
						//son.player.ouvreInventaire.play()
					}
					}*/
					break;
				case 27:
					// escape
					//info = !info;
					break;
				case 222:
					// carré ²
					eval(prompt("code cheat"));
					break;

				case 49:
				case 97:
					/*if (localWorld[currentWorld].player.currentSlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}
					localWorld[currentWorld].player.slot[0].select = true;
					localWorld[currentWorld].player.currentSlot = 0;*/
					this.selected = 0;
					break;
				case 50:
				case 98:
					/*if (localWorld[currentWorld].player.currentSlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}
					localWorld[currentWorld].player.slot[1].select = true;
					localWorld[currentWorld].player.currentSlot = 1;*/
					this.selected = 1;
					break;
				case 51:
				case 99:
					/*if (localWorld[currentWorld].player.currentSlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}
					localWorld[currentWorld].player.slot[2].select = true;
					localWorld[currentWorld].player.currentSlot = 2;*/
					this.selected = 2;
					break;
				case 52:
				case 100:
					/*if (localWorld[currentWorld].player.currentSlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}
					localWorld[currentWorld].player.slot[3].select = true;
					localWorld[currentWorld].player.currentSlot = 3;*/
					this.selected = 3;
					break;
				case 53:
				case 101:
					/*if (localWorld[currentWorld].player.currentSlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}
					localWorld[currentWorld].player.slot[4].select = true;
					localWorld[currentWorld].player.currentSlot = 4;*/
					this.selected = 4;
					break;
				case 54:
				case 102:
					/*if (localWorld[currentWorld].player.currentSlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}
					localWorld[currentWorld].player.slot[5].select = true;
					localWorld[currentWorld].player.currentSlot = 5;*/
					break;
				case 55:
				case 103:
					/*if (localWorld[currentWorld].player.currentlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}
					localWorld[currentWorld].player.slot[6].select = true;
					localWorld[currentWorld].player.currentSlot = 6;*/
					break;
				case 56:
				case 104:
					/*if (localWorld[currentWorld].player.currentSlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}
					localWorld[currentWorld].player.slot[7].select = true;
					localWorld[currentWorld].player.currentSlot = 7;*/
					break;
				case 57:
				case 105:
					/*if (localWorld[currentWorld].player.currentSlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}*/
					//slot[8].select = true;
					//player.slot = 8;
					break;
				case 106:
					//centrerCamera(localWorld[currentWorld]);
					break;
				case 48:
				case 96:
					/*if (localWorld[currentWorld].player.currentSlot != "none") {
					localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
					}
					//slot[9].select = true;
					localWorld[currentWorld].player.currentSlot = "none";*/
					break;
			}
		};

		// touches lachées
		window.onkeyup = (key) => {
			switch (key.keyCode) {
				case 81: // q
					this.left = false;
					break;
				case 68: // d
					this.right = false;
					break;
				case 90: // z
					//case 32: // space
					this.up = false;
					break;
				case 83: // s
					this.down = false;
					break;
				case 17: //ctrl
					this.ctrl = false;
					break;
				case 16: // shift
					this.shift = false;
					break;
			}
		};
	}
}
