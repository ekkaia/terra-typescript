import { Position, Velocity, Acceleration, Size } from "./Data";
import { Canvas } from "./Canvas";
import { Camera } from "./Camera";
import { Chunk } from "./Chunk";
import { Dimension } from "./Dimension";

export abstract class Entity {
	public position: Position;
	protected velocity: Velocity;
	protected acceleration: Acceleration;
	public size: Size;
	//public chunk: Chunk;
	public chunks: { left: number; right: number };
	public dimension: Dimension;

	constructor(pos: Position = { x: 0, y: 0 }, size: Size = { w: 0, h: 0 }, vel: Velocity = { x: 0, y: 0 }, acc: Acceleration = { x: 0, y: 0 }) {
		this.position = pos;
		this.velocity = vel;
		this.acceleration = acc;
		this.size = size;
	}

	public abstract isVisible(camera: Camera, size: Size): boolean;
	public abstract render(canvas: Canvas, camera: Camera, size: Size): void;
	public abstract update(): void;
	public abstract getMiddlePos(): Position;
	/*public setChunk(chunk: Chunk): void {
		this.chunk = chunk;
	}*/
	public setDimension(dimension: Dimension): void {
		this.dimension = dimension;
	}
}
