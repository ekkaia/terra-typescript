import { Canvas } from "./Canvas";
import { World } from "./World";
import { Input } from "./Input";
import { Player } from "./Player";

const backgroundCanvas: Canvas = new Canvas(<HTMLCanvasElement>document.getElementById("backgroundRender"));
backgroundCanvas.filter("brightness(50%)");
const foregroundCanvas: Canvas = new Canvas(<HTMLCanvasElement>document.getElementById("foregroundRender"));
const entitiesCanvas: Canvas = new Canvas(<HTMLCanvasElement>document.getElementById("entitiesRender"));

const player: Player = new Player("BROCHARD", { w: 0.8, h: 1.4 }, { x: 0, y: 0 });
const world: World = new World(/*player*/);
player.setDimension(world.dimension[world.active]);
world.spawnPlayer(player);
export const input: Input = new Input(world);
player.setInput(input);

function loop() {
	backgroundCanvas.update();
	backgroundCanvas.filter("brightness(50%)");
	foregroundCanvas.update();
	entitiesCanvas.update();

	entitiesCanvas.update();
	input.update();
	//player.update();
	world.update();
	world.render(backgroundCanvas, foregroundCanvas, entitiesCanvas);
	//player.render(canvas, world.dim[world.active].cam);
	if (world.minimapTest) {
		entitiesCanvas.minimapTest(world.dimension[world.active]);
	}
}

entitiesCanvas.frameRate(loop, 60);
