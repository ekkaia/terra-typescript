import { Block } from "./Block";
import { Camera } from "../Camera";
import { Canvas } from "../Canvas";
import { Chunk } from "../Chunk";
import { Size, Position } from "../Data";
import { Dimension } from "../Dimension";

export class Air extends Block {
	constructor(position: Position, dimension: Dimension, clicked?: Position) {
		super("Air", { r: 0, g: 0, b: 0, a: 0 }, position, dimension, 0, false, true, 0.1, 0.2, 0);
	}

	public render(canvas: Canvas, cam: Camera, size: Size): void {}

	public update(): void {}

	public randomUpdate(): void {}

	public remove(): Block {
		return this;
	}

	public isFullHere(playerPos: Position, playerSize: Size): boolean {
		return true;
	} //Todo
}
