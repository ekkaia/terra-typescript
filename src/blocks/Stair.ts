import { Block } from "./Block";
import { Camera } from "../Camera";
import { Canvas } from "../Canvas";
import { Chunk } from "../Chunk";
import { Position, Size } from "../Data";
import { stair_bottomLeft, stair_bottomRight, stair_topLeft, stair_topRight } from "../Texture";
import { Air } from "./Air";
import { Grass } from "./Grass";
import { NonFullBlock } from "./NonFullBlock";
import { Circle, Point, Polygon, Rectangle } from "../Shape";
import { Dimension } from "../Dimension";

export class Stair extends NonFullBlock {
	private texture;
	constructor(position: Position, dimension: Dimension, clicked: Position) {
		super("Stair", { r: 150, g: 75, b: 0, a: 255 }, position, dimension, 10, true, false, 0.5, 1, 0);

		// stair (right doesn't work)
		/*this.oldboxtest.push({ from: { x: 0, y: 1 }, to: { x: 0.5, y: 1 } });
		this.oldboxtest.push({ from: { x: 0.5, y: 1 }, to: { x: 0.5, y: 0.5 } });
		this.oldboxtest.push({ from: { x: 0.5, y: 0.5 }, to: { x: 1, y: 0.5 } });
		this.oldboxtest.push({ from: { x: 1, y: 0.5 }, to: { x: 1, y: 0 } });
		this.oldboxtest.push({ from: { x: 1, y: 0 }, to: { x: 0, y: 0 } });
		this.oldboxtest.push({ from: { x: 0, y: 0 }, to: { x: 0, y: 1 } });*/

		// fence (top and left works)
		/*this.oldboxtest.push({ from: { x: 0.3, y: 1 }, to: { x: 0.6, y: 1 } });
		this.oldboxtest.push({ from: { x: 0.6, y: 1 }, to: { x: 0.6, y: 0 } });
		this.oldboxtest.push({ from: { x: 0.6, y: 0 }, to: { x: 0.3, y: 0 } });
		this.oldboxtest.push({ from: { x: 0.3, y: 0 }, to: { x: 0.3, y: 1 } });*/

		// triangle ()
		/*this.oldboxtest.push({ from: { x: 0.5, y: 1 }, to: { x: 1, y: 0 } });
		this.oldboxtest.push({ from: { x: 1, y: 0 }, to: { x: 0, y: 0 } });
		this.oldboxtest.push({ from: { x: 0, y: 0 }, to: { x: 0.5, y: 1 } });*/

		// losange (only top left works) swaped
		/*this.oldboxtest.push({ from: { x: 0.5, y: 1 }, to: { x: 1, y: 0.5 } });
		this.oldboxtest.push({ from: { x: 0.5, y: 0 }, to: { x: 1, y: 0.5 } });
		this.oldboxtest.push({ from: { x: 0.5, y: 0 }, to: { x: 0, y: 0.5 } }); 
		this.oldboxtest.push({ from: { x: 0, y: 0.5 }, to: { x: 0.5, y: 1 } });*/

		// ramp (bottom right doesn't work)
		/*this.oldboxtest.push({ from: { x: 0, y: 0 }, to: { x: 1, y: 1 } });
		this.oldboxtest.push({ from: { x: 1, y: 1 }, to: { x: 1, y: 0 } });
		this.oldboxtest.push({ from: { x: 1, y: 0 }, to: { x: 0, y: 0 } });*/

		// slab
		/*this.oldboxtest.push({ from: { x: 0, y: 0.5 }, to: { x: 1, y: 0.5 } });
		this.oldboxtest.push({ from: { x: 1, y: 0 }, to: { x: 1, y: 0.5 } });
		this.oldboxtest.push({ from: { x: 0, y: 0 }, to: { x: 1, y: 0 } });
		this.oldboxtest.push({ from: { x: 0, y: 0 }, to: { x: 0, y: 0.5 } });*/

		//const x: number = Math.random();
		//const y: number = Math.random();
		/*this.hitbox.push(
			//new Rectangle({x: x, y: y}, {w: Math.random()*(1-x), h: Math.random()*(1-y)})

			new Rectangle({x: 0, y: 0}, {w: 1, h: 0.5}),
			new Rectangle({x: 0, y: 0.5}, {w: 0.5, h: 0.5})

			// ramp
			new Polygon([
				new Point({x: 0, y: 0}),
				new Point({x: 0, y: 1}),
				new Point({x: 1, y: 0})
			])

			// diamond
			new Polygon([
				new Point({x: 0.5, y: 0}),
				new Point({x: 0, y: 0.5}),
				new Point({x: 0.5, y: 1}),
				new Point({x: 1, y: 0.5})
			])

			// stair
			new Polygon([
				new Point({x: 0, y: 0}),
				new Point({x: 0, y: 1}),
				new Point({x: 0.5, y: 1}),
				new Point({x: 0.5, y: 0.5}),
				new Point({x: 1, y: 0.5}),
				new Point({x: 1, y: 0})
			])

			// box
			new Polygon([
				new Point({x: 0, y: 0}),
				new Point({x: 0, y: 1}),
				new Point({x: 1, y: 1}),
				new Point({x: 1, y: 0.3}),
				new Point({x: 0.9, y: 0.3}),
				new Point({x: 0.9, y: 0.9}),
				new Point({x: 0.1, y: 0.9}),
				new Point({x: 0.1, y: 0.1}),
				new Point({x: 1, y: 0.1}),
				new Point({x: 1, y: 0}),
			])

			//new Circle({ x: 0.5, y: 0.5 }, 0.5)
		);*/

		if(clicked.x < 0.5) {
			if(clicked.y < 0.5) {
				// bottomLeft
				console.log("bottomLeft");
				this.texture = stair_bottomLeft.texture;
				this.hitbox.push(
					new Rectangle({x: 0, y: 0}, {w: 1, h: 0.5}),
					new Rectangle({x: 0, y: 0.5}, {w: 0.5, h: 0.5})
				);
			}
			else {
				// topLeft
				console.log("topLeft");
				this.texture = stair_topLeft.texture;
				this.hitbox.push(
					new Rectangle({x: 0, y: 0.5}, {w: 1, h: 0.5}),
					new Rectangle({x: 0, y: 0}, {w: 0.5, h: 0.5})
				);
			}
		}
		else {
			if(clicked.y < 0.5) {
				// bottomRight
				console.log("bottomRight");
				this.texture = stair_bottomRight.texture;
				this.hitbox.push(
					new Rectangle({x: 0, y: 0}, {w: 1, h: 0.5}),
					new Rectangle({x: 0.5, y: 0.5}, {w: 0.5, h: 0.5})
				);
			}
			else {
				// topRight
				console.log("topRight");
				this.texture = stair_topRight.texture;
				this.hitbox.push(
					new Rectangle({x: 0, y: 0.5}, {w: 1, h: 0.5}),
					new Rectangle({x: 0.5, y: 0}, {w: 0.5, h: 0.5})
				);
			}
		}
		
		/*this.hitbox.push(
			new Rectangle({x: 0, y: 0}, {w: 1, h: 0.5}),
			new Rectangle({x: 0, y: 0.5}, {w: 0.5, h: 0.5})
		);*/
		// TODO: SEGMENTS MUST GO FROM MIN TO MAX VALUE OR COLLIDE DOESNT WORK
		this.addUpdate();
	}

	public render(canvas: Canvas, cam: Camera, size: Size): void {
		canvas.image(
			this.texture,
			{ x: this.position.x * size.w - cam.pos.x, y: -this.position.y * size.h - cam.pos.y },
			{ w: size.w, h: size.h }
		);
		canvas.fill("black");
		canvas.stroke("Red");
		/*this.oldboxtest.forEach((segment) => {
			const relativeSegment = this.relativeSegment(segment);
			canvas.line({
				from: { x: relativeSegment.from.x * size.w - cam.pos.x, y: -(relativeSegment.from.y - 1) * size.h - cam.pos.y },
				to: { x: relativeSegment.to.x * size.w - cam.pos.x, y: -(relativeSegment.to.y - 1) * size.h - cam.pos.y },
			});
		});*/
		this.hitbox.forEach((shape) => {
			shape.draw(canvas, this.position, cam, size);
		});
	}

	public update(): void {}

	public randomUpdate(): void {}

	public remove(): Block {
		this.dimension.setBlock(new Air(this.position, this.dimension));
		return this;
	}

	/*public isFullHere(scope: { from: Position; to: Position }): boolean {
		//alert(playerPos.x + " " + this.position.x);
		//Todo: Player pos should be the moving pos
		/*console.log(
			(-playerPos.y + playerSize.y < this.position.y + 0.5 && playerPos.x <= this.position.x + 1) +
				" " +
				(-playerPos.y + playerSize.y >= this.position.y + 0.5 && playerPos.x <= this.position.x + 0.5) ||
				(playerPos.x + playerSize.x < this.position.x + 0.5 && -playerPos.y <= this.position.y + 1) ||
				(playerPos.x + playerSize.x >= this.position.x + 0.5 && -playerPos.y <= this.position.y + 0.5)
		);
		return ((playerPos.x + playerSize.x >= this.position.x)
			|| (playerPos.y + playerSize.y >= this.position.y + 0.5)
			|| (playerPos.x >= this.position.x + 0.5)
			);
		return (
			(-playerPos.y + playerSize.y < this.position.y + 0.5 && playerPos.x <= this.position.x + 1) ||
			(-playerPos.y + playerSize.y >= this.position.y + 0.5 && playerPos.x <= this.position.x + 0.5) ||
			(playerPos.x + playerSize.x < this.position.x + 0.5 && -playerPos.y <= this.position.y + 1) ||
			(playerPos.x + playerSize.x >= this.position.x + 0.5 && -playerPos.y <= this.position.y + 0.5)
		);
		return true;
	}*/
}
