import { Block } from "./Block";
import { Camera } from "../Camera";
import { Canvas } from "../Canvas";
import { Chunk } from "../Chunk";
import { Size, Position } from "../Data";
import { Liquid } from "../Liquid";
import { Air } from "./Air";
import { Dimension } from "../Dimension";

export class Water extends Block {
	//public level: number;
	//public quantity: number;
	//public blended: Water;
	public liquid: Liquid;

	constructor(position: Position, dimension: Dimension, liquid: Liquid, clicked?: Position) {
		super("Water", { r: 0, g: 0, b: 255, a: 255 }, position, dimension, 15, false, true, 0.25, 0, 0);
		//this.level = 100;
		//this.quantity = 1;
		this.liquid = liquid;
		this.liquid.add(this);
		//this.blended = undefined;
		this.addUpdate();
	}

	public render(canvas: Canvas, cam: Camera, size: Size): void {
		canvas.fill("blue");
		canvas.fillRect(
			{
				x: this.position.x * size.w - cam.pos.x,
				y: -this.position.y * size.h - cam.pos.y + size.h - ((this.liquid.volume / 100) * size.h) / this.liquid.quantity,
			},
			{ w: size.w, h: (size.h * this.liquid.volume) / 100 / this.liquid.quantity }
		);
	}

	public update(): void {
		//this.needsUpdate = this.needsUpdate || this.liquid.needsUpdate;

		let didSomething = false;

		if (this.liquid.merged != undefined) {
			this.liquid.remove(this);
			this.liquid = this.liquid.merged;
			this.liquid.add(this);
			didSomething = true;
			//this.liquid.needsUpdate = true;
		}

		if (this.liquid.volume == 0) {
			this.liquid.remove(this);
			this.dimension.setBlock(new Air(this.position, this.dimension));
		} else {
			//console.log(this.level);
			didSomething = didSomething || this.spread();
			didSomething = didSomething || this.blendNeighbor();

			if (didSomething) {
				this.updateSurroundings();
			}

			if (didSomething) {
				this.addUpdate();
				this.liquid.water.forEach((water) => {
					water.addUpdate();
				});
			}
		}

		//console.log(this.liquid.volume);
	}

	public blendNeighbor(): boolean {
		const leftBlock: Block = this.dimension.getBlock({ x: this.position.x - 1, y: this.position.y });
		const rightBlock: Block = this.dimension.getBlock({ x: this.position.x + 1, y: this.position.y });
		let didSomething = false;

		if (leftBlock instanceof Water && leftBlock.liquid != this.liquid) {
			didSomething = true;
			leftBlock.blend(this, this.dimension, { x: this.position.x - 1, y: this.position.y });
			leftBlock.addUpdate();
		}

		if (rightBlock instanceof Water && rightBlock.liquid != this.liquid) {
			didSomething = true;
			rightBlock.blend(this, this.dimension, { x: this.position.x + 1, y: this.position.y });
			rightBlock.addUpdate();
		}

		return didSomething;
	}

	public blend(water: Water, dimension: Dimension, position: Position): void {
		//this.blended = water;
		//this.blended.liquid.quantity += this.liquid.quantity;
		//this.blended.liquid.volume += this.liquid.volume;

		//water.needsUpdate = true;
		//this.needsUpdate = true;
		water.liquid.blend(this.liquid);

		this.liquid.remove(this);
		this.liquid = water.liquid;
		this.liquid.add(this);

		dimension.updateBlock({ x: position.x, y: position.y + 1 });
		//console.log({x: position.x, y: position.y+1});
	}

	public freeFall(): void {
		if (this.liquid.quantity == 1) {
			this.dimension.setBlock(new Air(this.position, this.dimension));
			this.position.y--;
			this.dimension.setBlock(this);
		} else {
			if (this.liquid.volume >= 100) {
				this.dimension.setBlock(new Water({ x: this.position.x, y: this.position.y - 1 }, this.dimension, new Liquid()));
				this.liquid.volume -= 100;
				this.liquid.remove(this);
			} else {
				this.dimension.setBlock(new Water({ x: this.position.x, y: this.position.y - 1 }, this.dimension, new Liquid(this.liquid.volume)));
				this.liquid.volume = 0;
				this.liquid.remove(this);
			}
		}
	}

	public fallInWater(bottomBlock: Water): void {
		if (bottomBlock.liquid.volume + this.liquid.volume < bottomBlock.liquid.quantity * 100) {
			bottomBlock.liquid.volume += this.liquid.volume;
			this.liquid.volume = 0;
			this.liquid.remove(this);
		} else {
			this.liquid.volume -= bottomBlock.liquid.quantity * 100 - bottomBlock.liquid.volume;
			bottomBlock.liquid.volume = bottomBlock.liquid.quantity * 100;
		}
		bottomBlock.addUpdate();
	}

	public spread(): boolean {
		const bottomBlock: Block = this.dimension.getBlock({
			x: this.position.x,
			y: this.position.y - 1,
		});
		let didSomething = false;
		// if there is no block bellow
		if (bottomBlock instanceof Air) {
			didSomething = true;
			this.freeFall();
			// if there is water bellow
		} else if (bottomBlock instanceof Water && bottomBlock.liquid.volume < 100 * bottomBlock.liquid.quantity) {
			didSomething = true;
			this.fallInWater(bottomBlock);
		} else {
			didSomething = this.spreadSides();
		}
		return didSomething;
	}

	public spreadSides(): boolean {
		let didSomething = false;
		if (this.dimension.getBlock({ x: this.position.x - 1, y: this.position.y }) instanceof Air) {
			didSomething = true;
			this.dimension.setBlock(new Water({ x: this.position.x - 1, y: this.position.y }, this.dimension, this.liquid));
			//chunk.updateBlock({ x: position.x - 1, y: position.y+1 });
			this.liquid.quantity++;
		}
		if (this.dimension.getBlock({ x: this.position.x + 1, y: this.position.y }) instanceof Air) {
			didSomething = true;
			this.dimension.setBlock(new Water({ x: this.position.x + 1, y: this.position.y }, this.dimension, this.liquid));
			//chunk.updateBlock({ x: position.x - 1, y: position.y+1 });s
			this.liquid.quantity++;
		}
		return didSomething;
	}

	public randomUpdate(): void {}

	public remove(): Block {
		if (this.liquid.volume >= 100) {
			this.liquid.volume -= 100;
			return new Water(this.position, this.dimension, new Liquid());
		} else {
			const out: Water = new Water(this.position, this.dimension, new Liquid(this.liquid.volume));
			this.liquid.volume = 0;
			return out;
		}
	}

	public isFullHere(playerPos: Position, playerSize: Size): boolean {
		return true;
	} //Todo
}
