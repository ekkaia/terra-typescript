import { Dirt } from "./Dirt";
import { Grass } from "./Grass";
import { Ice } from "./Ice";
import { IceTop } from "./IceTop";
import { Stone } from "./Stone";

export type BlockType = typeof Grass | typeof Stone | typeof Dirt | typeof Ice | typeof IceTop;
