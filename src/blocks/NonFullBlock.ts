import { Chunk } from "../Chunk";
import { Color, Position } from "../Data";
import { Dimension } from "../Dimension";
import { Shape } from "../Shape";
import { Block } from "./Block";

export abstract class NonFullBlock extends Block {
	//readonly oldboxtest: { from: Position; to: Position }[] = [];
	readonly hitbox: Shape[] = [];
	constructor(
		name: string,
		color: Color,
		position: Position,
		dimension: Dimension,
		durability: number,
		isSolid: boolean,
		isTransparent: boolean,
		drag: number,
		friction: number,
		bounciness: number
	) {
		super(name, color, position, dimension, durability, isSolid, isTransparent, drag, friction, bounciness);
	}

	public relativeSegment(segment: { from: Position; to: Position }): { from: Position; to: Position } {
		return {
			from: { x: segment.from.x + this.position.x, y: segment.from.y + this.position.y },
			to: { x: segment.to.x + this.position.x, y: segment.to.y + this.position.y },
		};
	}

	/*public checkVertical(check: { from: Position; to: Position }): boolean {
		//let inter: Segment = {pos1: {x: x, y:1}, pos2: {x:x, y:1}};
		const top = 0;
		let colide = false;
		this.oldboxtest.forEach((segment) => {
			const relativeSegment = this.relativeSegment(segment);
			//if(relativeSegment.from.x <= check.from.x && relativeSegment.to.x >= check.from.x || relativeSegment.from.x >= check.from.x && relativeSegment.to.x <= check.from.x) {
			if (this.areSegmentIntersecting(check, relativeSegment)) {
				colide = true;
				//console.log(JSON.stringify(check) + " " + JSON.stringify(relativeSegment));
			}
			//let det: number = segment.from.x * segment.to.y - segment.from.y * segment.to.x
		});
		//console.log("vertical" + colide);
		return colide;
	}

	public checkHorizontal(check: { from: Position; to: Position }): boolean {
		//let inter: Segment = {pos1: {x: x, y:1}, pos2: {x:x, y:1}};
		const top = 0;
		let colide = false;
		this.oldboxtest.forEach((segment) => {
			const relativeSegment = this.relativeSegment(segment);
			//if(relativeSegment.from.y <= check.from.y && relativeSegment.to.y >= check.from.y || relativeSegment.from.y >= check.from.y && relativeSegment.to.y <= check.from.y) {
			if (this.areSegmentIntersecting(check, relativeSegment)) {
				colide = true;
				console.log(JSON.stringify(check) + " " + JSON.stringify(relativeSegment));
			}
			//let det: number = segment.from.x * segment.to.y - segment.from.y * segment.to.x
		});
		//console.log("horizontal" + colide);
		return colide;
	}

	public checkold(check: { from: Position; to: Position }): boolean {
		//console.log("check");
		let colide = false;
		const test = 0;
		this.oldboxtest.forEach((segment) => {
			const relativeSegment: { from: Position; to: Position } = this.relativeSegment(segment);
			//check.forEach((checks) => {
			if (this.areSegmentIntersecting(check, relativeSegment)) {
				colide = true;
				//console.log(test + " " + this.segments.length + " " + JSON.stringify(check) + " " + JSON.stringify(relativeSegment));
				//test++;
				//console.log(i+1 + " / " + this.segments.length);
				return true;
			}
			//});
		});
		//for(let i: number = 0; i < this.segments.length; i++) {
		//	let relativeSegment: Segment = this.relativeSegment(this.segments[i]);
		//	if (this.areSegmentIntersecting(check, relativeSegment)) {
				//console.log(i+1 + " / " + this.segments.length);
		//		return true;
		//	}
		//}
		return colide;
	}*/

	public check(entityHitbox: Shape): boolean {
		let collide = false;
		this.hitbox.forEach((shape1) => {
			//entityHitbox.forEach((shape2) => {
			//! console.log(shape1.relative(this.position).toString());
			//! console.log(entityHitbox.toString());
			if (Shape.collide(shape1.relative(this.position), entityHitbox)) collide = true;
			//});
		});
		return collide;
	}

	public areSegmentIntersecting(seg1: { from: Position; to: Position }, seg2: { from: Position; to: Position }): boolean {
		const eps = 0.0000001;
		const between = (a, b, c) => a - eps <= b && b <= c + eps;
		const angle = (s: { from: Position; to: Position }) => Math.atan2(s.to.y - s.from.y, s.to.x - s.from.x);
		const x =
			((seg1.from.x * seg1.to.y - seg1.from.y * seg1.to.x) * (seg2.from.x - seg2.to.x) -
				(seg1.from.x - seg1.to.x) * (seg2.from.x * seg2.to.y - seg2.from.y * seg2.to.x)) /
			((seg1.from.x - seg1.to.x) * (seg2.from.y - seg2.to.y) - (seg1.from.y - seg1.to.y) * (seg2.from.x - seg2.to.x));

		const y =
			((seg1.from.x * seg1.to.y - seg1.from.y * seg1.to.x) * (seg2.from.y - seg2.to.y) -
				(seg1.from.y - seg1.to.y) * (seg2.from.x * seg2.to.y - seg2.from.y * seg2.to.x)) /
			((seg1.from.x - seg1.to.x) * (seg2.from.y - seg2.to.y) - (seg1.from.y - seg1.to.y) * (seg2.from.x - seg2.to.x));

		/*if( ((seg1.from.x - seg1.to.x) * (seg2.from.y - seg2.to.y) - (seg1.from.y - seg1.to.y) * (seg2.from.x - seg2.to.x)) == 0 ) {
			if(!isNaN(x) && !isNaN(y)) {
				console.log(x + " " + y);
			}
			else if(!isNaN(x)) {
				console.log("x: " + x);
			}
			else if(!isNaN(y)) {
				console.log("y: " + y);
			}
		}*/

		if (isNaN(x) || isNaN(y)) {
			return false; // temporary
			if (angle(seg1) == angle(seg2)) {
				if (between(seg1.from.x, seg2.from.x, seg1.to.x) || between(seg1.to.x, seg2.from.x, seg1.from.x)) {
					console.log("x");
					return true;
				} else if (between(seg1.from.y, seg2.from.y, seg1.to.y) || between(seg1.to.y, seg2.from.y, seg1.from.y)) {
					console.log("y");
					return true;
				} else {
					return false;
				}
			}
			console.log("does it ever happen ?");
			return false;
		} else if (
			(seg1.from.x >= seg1.to.x && !between(seg1.to.x, x, seg1.from.x)) ||
			!between(seg1.from.x, x, seg1.to.x) ||
			(seg1.from.y >= seg1.to.y && !between(seg1.to.y, y, seg1.from.y)) ||
			!between(seg1.from.y, y, seg1.to.y) ||
			(seg2.from.x >= seg2.to.x && !between(seg2.to.x, x, seg2.from.x)) ||
			!between(seg2.from.x, x, seg2.to.x) ||
			(seg2.from.y >= seg2.to.y && !between(seg2.to.y, y, seg2.from.y)) ||
			!between(seg2.from.y, y, seg2.to.y)
		) {
			return false;
		}

		//return {x: x, y: y};
		//console.log(x + " " + y);
		return true;
	}

	public isFullHere(scope: { from: Position; to: Position }): boolean {
		return true;
	}
}
