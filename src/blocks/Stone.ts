import { Block } from "./Block";
import { Camera } from "../Camera";
import { Canvas } from "../Canvas";
import { Chunk } from "../Chunk";
import { Position, Size } from "../Data";
import { stone } from "../Texture";
import { Air } from "./Air";
import { Dimension } from "../Dimension";

export class Stone extends Block {
	constructor(position: Position, dimension: Dimension, clicked?: Position) {
		super("Stone", { r: 100, g: 100, b: 100, a: 255 }, position, dimension, 10, true, false, 0.5, 1, 0);
		this.addUpdate();
	}

	public render(canvas: Canvas, cam: Camera, size: Size): void {
		canvas.image(stone.texture, { x: this.position.x * size.w - cam.pos.x, y: -this.position.y * size.h - cam.pos.y }, { w: size.w, h: size.h });
	}

	public update(): void {
		if (Math.floor(Math.random() * 1000) == 1) {
			this.dimension.setBlock(new Stone(this.position, this.dimension));
		}
	}

	public randomUpdate(): void {}

	public remove(): Block {
		this.dimension.setBlock(new Air(this.position, this.dimension));
		return this;
	}

	public isFullHere(playerPos: Position, playerSize: Size): boolean {
		return true;
	} //Todo
}
