import { Camera } from "../Camera";
import { Canvas } from "../Canvas";
import { Chunk } from "../Chunk";
import { Color, Position, Size } from "../Data";
import { Dimension } from "../Dimension";

export abstract class Block {
	readonly name: string;
	public position: Position;
	public dimension: Dimension;
	readonly color: Color;
	private durability: number;
	private miningProgress: number;
	public readonly isSolid: boolean;
	public readonly isTransparent: boolean;
	public drag: number;
	public friction: number;
	public bounciness: number;

	constructor(
		name: string,
		color: Color,
		position: Position,
		dimension: Dimension,
		durability: number,
		isSolid: boolean,
		isTransparent: boolean,
		drag: number,
		friction: number,
		bounciness: number
	) {
		this.name = name;
		this.color = color;
		this.position = position;
		this.dimension = dimension;
		this.durability = durability;
		this.miningProgress = 0;
		this.isSolid = isSolid;
		this.isTransparent = isTransparent;
		this.drag = drag;
		this.friction = friction;
		this.bounciness = bounciness;
	}

	public abstract render(canvas: Canvas, camera: Camera, size: Size): void;

	public abstract update(): void;
	public updateSurroundings(): void {
		this.dimension.updateBlock({ x: this.position.x - 1, y: this.position.y });
		this.dimension.updateBlock({ x: this.position.x, y: this.position.y + 1 });
		this.dimension.updateBlock({ x: this.position.x, y: this.position.y - 1 });
		this.dimension.updateBlock({ x: this.position.x + 1, y: this.position.y });
	}

	public abstract randomUpdate(): void;
	public abstract remove(): Block;
	public addUpdate(): void {
		this.dimension.addUpdate(this);
	}
}
