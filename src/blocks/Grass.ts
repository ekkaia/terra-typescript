import { Block } from "./Block";
import { Camera } from "../Camera";
import { Canvas } from "../Canvas";
import { Chunk } from "../Chunk";
import { Size, Position } from "../Data";
import { grass } from "../Texture";
import { Air } from "./Air";
import { Dirt } from "./Dirt";
import { Dimension } from "../Dimension";

export class Grass extends Block {
	constructor(position: Position, dimension: Dimension, clicked?: Position) {
		super("Grass", { r: 0, g: 255, b: 0, a: 255 }, position, dimension, 15, true, false, 0.5, 1, 0);
	}

	public render(canvas: Canvas, cam: Camera, size: Size): void {
		canvas.image(grass.texture, { x: this.position.x * size.w - cam.pos.x, y: -this.position.y * size.h - cam.pos.y }, { w: size.w, h: size.h });
	}

	public update(): void {
		return null;
	}

	public randomUpdate(): void {
		const top: Block = this.dimension.getBlock({ x: this.position.x, y: this.position.y + 1 });

		if (!(top instanceof Air)) {
			this.dimension.setBlock(new Dirt(this.position, this.dimension));
		} else {
			const topLeft: Block = this.dimension.getBlock({ x: this.position.x - 1, y: this.position.y + 1 });
			const left: Block = this.dimension.getBlock({ x: this.position.x - 1, y: this.position.y });
			const bottomLeft: Block = this.dimension.getBlock({ x: this.position.x - 1, y: this.position.y - 1 });
			const topRight: Block = this.dimension.getBlock({ x: this.position.x + 1, y: this.position.y + 1 });
			const right: Block = this.dimension.getBlock({ x: this.position.x + 1, y: this.position.y });
			const bottomRight: Block = this.dimension.getBlock({ x: this.position.x + 1, y: this.position.y - 1 });

			if (topLeft instanceof Dirt) {
				topLeft.growthAttempt();
				topLeft.addUpdate();
			}
			if (left instanceof Dirt) {
				left.growthAttempt();
				left.addUpdate();
			}
			if (bottomLeft instanceof Dirt) {
				bottomLeft.growthAttempt();
				bottomLeft.addUpdate();
			}
			if (topRight instanceof Dirt) {
				topRight.growthAttempt();
				topRight.addUpdate();
			}
			if (right instanceof Dirt) {
				right.growthAttempt();
				right.addUpdate();
			}
			if (bottomRight instanceof Dirt) {
				bottomRight.growthAttempt();
				bottomRight.addUpdate();
			}
		}
	}

	public remove(): Block {
		this.dimension.setBlock(new Air(this.position, this.dimension));
		return this;
	}

	public isFullHere(playerPos: Position, playerSize: Size): boolean {
		return true;
	} //Todo
}
