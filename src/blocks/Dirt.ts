import { Block } from "./Block";
import { Camera } from "../Camera";
import { Canvas } from "../Canvas";
import { Chunk } from "../Chunk";
import { Position, Size } from "../Data";
import { dirt } from "../Texture";
import { Air } from "./Air";
import { Grass } from "./Grass";
import { Dimension } from "../Dimension";

export class Dirt extends Block {
	constructor(position: Position, dimension: Dimension, clicked?: Position) {
		super("Dirt", { r: 150, g: 75, b: 0, a: 255 }, position, dimension, 10, true, false, 0.5, 1, 0);
		this.addUpdate();
	}

	public render(canvas: Canvas, cam: Camera, size: Size): void {
		canvas.image(dirt.texture, { x: this.position.x * size.w - cam.pos.x, y: -this.position.y * size.h - cam.pos.y }, { w: size.w, h: size.h });
	}

	public update(): void {
		/*if (Math.floor(Math.random() * 1000) == 1) {
			this.chunk.setBlock(new Grass(this.position, this.chunk));
		}*/
	}

	public randomUpdate(): void {
		/*if(this.chunk.getBlock({x: this.position.x, y: this.position.y+1}) instanceof Air) {
			this.chunk.setBlock(new Grass(this.position, this.chunk));
			this.addUpdate();
		}*/
	}

	public remove(): Block {
		this.dimension.setBlock(new Air(this.position, this.dimension));
		return this;
	}

	public growthAttempt() {
		if (this.dimension.getBlock({ x: this.position.x, y: this.position.y + 1 }) instanceof Air && Math.floor(Math.random() * 100) == 1) {
			this.dimension.setBlock(new Grass(this.position, this.dimension));
		}
	}

	public isFullHere(playerPos: Position, playerSize: Size): boolean {
		return true;
	} //Todo
}
