import { Canvas } from "./Canvas";
import { Player } from "./Player";
import { Dimension } from "./Dimension";
import { Air } from "./blocks/Air";

export class World {
	public dimension: Dimension[];
	public active: number;
	public player: Player = undefined;
	public minimapTest = false;

	constructor(/*player: Player*/) {
		this.dimension = [];
		this.active = 0;
		this.dimension.push(new Dimension({ w: 25, h: 25 }));
		this.dimension[this.active].centerCamera();
		//this.player = player;
		//this.dimension[this.active].addEntity(0, this.player);
		//this.dimension[this.active].spawnPlayer(this.player);
	}

	public test(): void {
		this.dimension[0].newChunkRight();
		this.dimension[0].newChunkLeft();
		//console.log(JSON.stringify(Object.assign(this)));
		//console.log(JSON.stringify(this));
	}

	public spawnPlayer(player: Player): void {
		this.player = player;
		this.dimension[this.active].spawnPlayer(player);
	}

	public leftClicked(): void {
		if (this.player != undefined) {
			if (this.player.getBlock() instanceof Air == false) this.player.removeBlock();
			// TODO: can't put "!" ???
		}
	}

	public rightClicked(placing: any): void {
		if (this.player != undefined) {
			this.player.placeBlock(placing);
		}
	}

	public update(): void {
		if (this.player != undefined) this.dimension[this.active].centerCamera(this.player);
		this.dimension[this.active].update();
	}

	public render(backgroundCanvas: Canvas, foregroundCanvas: Canvas, entitiesCanvas: Canvas): void {
		this.dimension[this.active].render(backgroundCanvas, foregroundCanvas, entitiesCanvas);
	}
}
