import { Air } from "./blocks/Air";
import { Color, Position, Size } from "./Data";
import { Dimension } from "./Dimension";

export class Canvas {
	private repeat: number;
	private ctx: CanvasRenderingContext2D;
	private canvas: HTMLCanvasElement;

	constructor(canvas: HTMLCanvasElement) {
		this.canvas = canvas;
		//this.canvas.push(<HTMLCanvasElement>document.getElementById("entityRender"));

		//this.canvas.forEach((canvas, i) => {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		this.ctx = this.canvas.getContext("2d");
		//});

		//console.log(this.canvas);
	}

	public clear(): void {
		/*const temp = this.ctx.fillStyle;
		this.ctx.fillStyle = "white";
		this.rect({ x: 0, y: 0 }, { x: this.canvas.width, y: this.canvas.height });
		this.ctx.fillStyle = temp;*/
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		//this.canvas[id].width = this.canvas[id].width;
	}

	public filter(filter: string): void {
		this.ctx.filter = filter;
	}

	public resetFilter(): void {
		this.ctx.filter = "none";
	}

	public circle(pos: Position, diameter: number): void {
		this.ctx.arc(pos.x, pos.y, diameter, 0, 2 * Math.PI, false);
	}

	public update(): void {
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
		this.clear();
	}

	public static loadImage(src: string): HTMLImageElement {
		const img = new Image();
		img.src = src;
		return img;
	}

	public image(img: HTMLImageElement, pos: Position, size: Size): void {
		this.ctx.drawImage(img, pos.x, pos.y, size.w, size.h);
	}

	public fill(color: string): void {
		this.ctx.fillStyle = color;
	}

	public noFill(): void {
		// pas d'intérieur
		// rgba("0,0,0,0") ?
	}

	/*public line(pos1: Position, pos2: Position): void {
		this.ctx.beginPath();
		this.ctx.moveTo(pos1.x, pos1.y);
		this.ctx.lineTo(pos2.x, pos2.y);
		this.ctx.stroke();
	}*/
	public line(segment: { from: Position; to: Position }): void {
		this.ctx.beginPath();
		this.ctx.moveTo(segment.from.x, segment.from.y);
		this.ctx.lineTo(segment.to.x, segment.to.y);
		this.ctx.stroke();
	}

	public stroke(color: string): void {
		this.ctx.strokeStyle = color;
	}

	public noStroke(): void {
		// pas de bordure
		// rgba("0,0,0,0") ?
	}

	public fillRect(pos: Position, size: Size): void {
		this.ctx.fillRect(pos.x, pos.y, size.w, size.h);
	}

	public rect(pos: Position, size: Size): void {
		this.ctx.strokeRect(pos.x, pos.y, size.w, size.h);
	}
	/*
	loadFont(url) {
		const font = new FontFace("pixelFont", "url(" + url + ")");
		font.load();
		return font;
	}
	//await font.load(); async

	textFont(font, size, desc) {
		ctx.font = size + " " + font.family + " " + desc;
	}*/

	/* textSize(size) {
		console.log("on m'appelle");
		let fontArgs = ctx.font.split(' ');
		let newSize = size + "px";
		ctx.font = newSize + ' ' + fontArgs[fontArgs.length - 1];
	}*/

	//let pixelFont = new FontFace('pixelFont', 'url(pixelfont.ttfqbikjdqs)');
	//ctx.font = '48px pixelFont';

	public text(txt: string, pos: Position): void {
		this.ctx.fillText(txt, pos.x, pos.y);
	}

	public translate(pos: Position): void {
		this.ctx.translate(pos.x, pos.y);
	}

	// CALCULS POUR ROTATION DEPUIS LE CENTRE
	public rotate(degrees: number): void {
		this.ctx.rotate((degrees * Math.PI) / 180);
	}

	public resetMatrix(): void {
		this.ctx.resetTransform();
	}

	public loadSound(url: string): HTMLAudioElement {
		return new Audio(url);
	}

	public frameRate(func: () => void, rate: number): number {
		if (typeof rate == "number") {
			this.repeat = setInterval(func, Math.floor(1000 / rate));
		} else {
			return this.repeat; // TODO : compteur de fps
		}
	}

	public getSize(): Size {
		return { w: this.canvas.width, h: this.canvas.height };
	}

	public minimapTest(dimension: Dimension) {
		// Todo: put map, foreground, dimension, spawn on public just for this = bad
		const canvasData = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
		dimension.map.forEach((chunk) => {
			chunk.foreground.forEach((block) => {
				if (!(block instanceof Air)) {
					// Todo: better solution please
					this.drawPixel(canvasData, dimension.spawn + chunk.id, this.canvas.height - block.position.y, block.color);
				}
			});
		});
		this.ctx.putImageData(canvasData, 0, 0);
	}

	private drawPixel(imageData: ImageData, x, y, color: Color) {
		const index = (x + y * this.canvas.width) * 4;

		imageData.data[index + 0] = color.r;
		imageData.data[index + 1] = color.g;
		imageData.data[index + 2] = color.b;
		imageData.data[index + 3] = color.a;
	}
}
