import { Block } from "./blocks/Block";
import { Air } from "./blocks/Air";
import { Camera } from "./Camera";
import { Canvas } from "./Canvas";
import { Position, Size } from "./Data";
import { Dimension } from "./Dimension";
import { Entity } from "./Entity";
import { Stone } from "./blocks/Stone";
import { Biome } from "./biomes/Biome";

export class Chunk {
	readonly id: number;
	public biome: Biome;
	readonly dimension: Dimension;

	public foreground: Block[] = [];
	private background: Block[] = [];
	protected entities /*: Entity[]*/ = new Set<Entity>();

	constructor(dimension: Dimension, id: number, biome: Biome) {
		//console.log("Created Chunk id " + id);
		this.id = id;
		this.dimension = dimension;
		this.biome = biome;
		// generate chunk
		const top = this.biome.getHeightAtPosition(id);
		const dirtLevel: number = 10 - Math.random() * 2;
		for (let i = 0; i < top; i++) {
			//let test: Block = { name: "test" + i }
			if (i === top - 1) {
				this.foreground.push(new this.biome.floorBlock({ x: this.id, y: i }, this.dimension));
				this.background.push(new this.biome.floorBlock({ x: this.id, y: i }, this.dimension));
			} else if (i > top - dirtLevel) {
				this.foreground.push(new this.biome.depthBlock({ x: this.id, y: i }, this.dimension));
				this.background.push(new this.biome.depthBlock({ x: this.id, y: i }, this.dimension));
			} else {
				this.foreground.push(new Stone({ x: this.id, y: i }, this.dimension));
				this.background.push(new Stone({ x: this.id, y: i }, this.dimension));
			}
		}
	}

	public getSize(): Size {
		return this.dimension.getSize();
	}

	public getCamera(): Camera {
		return this.dimension.getCamera();
	}

	public getForegroundHeight(): number {
		return this.foreground.length;
	}

	public addEntity(entity: Entity): void {
		//console.log("added entity to " + this.id);
		//entity.setChunk(this);
		this.entities.add(entity);
	}

	public removeEntity(entity: Entity): void {
		//const findEntity = (element) => element === entity;
		//this.entities.splice(this.entities.findIndex(findEntity), 1);
		this.entities.delete(entity);
	}

	/*public checkSolidScope(scope: { from: Position; to: Position }): boolean {
		let isSolid = false;
		if (scope.from.x == scope.to.x) {
			for (let i: number = scope.from.y; i <= scope.to.y; i++) {
				if (this.getBlock({ x: scope.from.x, y: i }).isSolid) {
					isSolid = true;
				}
			}
		} else {
			for (let i: number = scope.from.x; i <= scope.to.x; i++) {
				if (this.getBlock({ x: i, y: scope.from.y }).isSolid) {
					isSolid = true;
				}
			}
		}
		return isSolid;
	}*/

	public setBlock(block: Block): void {
		if (block.position.x == this.id) {
			//console.log("final place")
			if (block.position.y - this.foreground.length > 0) {
				//console.log( position.y - this.foreground.length )

				//const blocks: Block[] = new Array(position.y - this.foreground.length).fill(new Air());
				//this.foreground.push(...blocks);

				for (let i: number = this.foreground.length; i < block.position.y; i++) {
					this.foreground.push(new Air({ x: this.id, y: i }, this.dimension));
				}
			}
			this.foreground[block.position.y] = block;
		} else {
			//console.log("get to other chunk")
			this.dimension.setBlock(block);
		}
	}

	public removeBlock(y: number): void {
		this.foreground[y].remove();
		this.foreground[y].update();
	}

	public getBlock(y: number): Block {
		if (y < this.foreground.length) {
			return this.foreground[y];
		} else {
			// TODO: renvoyer de l'air ?
			//alert("undefined");
			return new Air({ x: this.id, y: y }, undefined);
		}
	}

	/*public removePlayer(): number {
		const isPlayer = (element) => element === player;
		this.entities.splice(
			this.entities.findIndex(isPlayer),
			1
		);
	}*/

	public render(backgroundCanvas: Canvas, foregroundCanvas: Canvas, cam: Camera, size: Size): void {
		for (
			let i: number = Math.max(-Math.floor((cam.pos.y + cam.size.h) / size.h), 0);
			i <= Math.min(-Math.floor(cam.pos.y / size.h) + 1, this.foreground.length - 1);
			i++
		) {
			this.foreground[i].render(foregroundCanvas, cam, size);
		}
		/*for (
			let i: number = Math.max(-Math.floor((cam.pos.y + cam.size.y) / size.y), 0);
			i <= Math.min(-Math.floor(cam.pos.y / size.y) + 1, Math.max(this.foreground.length-1, this.background.length-1) );
			i++
		) {
			this.foreground[i].render(foregroundCanvas, cam, size);
			if(this.foreground[i].isTransparent) {
				this.background[i].render(backgroundCanvas, cam, size);
			}
		}*/

		for (
			let i: number = Math.max(-Math.floor((cam.pos.y + cam.size.h) / size.h), 0);
			i <= Math.min(-Math.floor(cam.pos.y / size.h) + 1, this.foreground.length - 1);
			i++
		) {
			if (this.entities.size != 0) {
				foregroundCanvas.fill("rgba(0,255,0,0.5)");
				foregroundCanvas.fillRect({ x: this.id * size.w - cam.pos.x, y: -i * size.h - cam.pos.y }, size);
			}
		}
	}

	updateEntities(updated: Set<Entity>): void {
		this.entities.forEach((entity) => {
			if (!updated.has(entity)) {
				entity.update();
				updated.add(entity);
			}
		});
	}

	renderEntities(entitiesCanvas: Canvas, camera: Camera, size: Size, rendered: Set<Entity>): void {
		this.entities.forEach((entity) => {
			if (!rendered.has(entity) && entity.isVisible(camera, size)) {
				entity.render(entitiesCanvas, camera, size);
				rendered.add(entity);
			}
		});
	}

	/*update(camera: Camera, size: Size): void {
		// make all updates
		for (
			let i: number = Math.max(-Math.floor((camera.pos.y + camera.size.y) / size.y), 0);
			i <= Math.min(-Math.floor(camera.pos.y / size.y) + 1, this.foreground.length - 1);
			i++
		) {
			if (this.foreground[i].needsUpdate) {
				this.foreground[i].update();
			}
		}
	}*/

	public updateBlock(position: Position): void {
		if (position.x == this.id) {
			if (position.y < this.foreground.length) {
				this.foreground[position.y].addUpdate();
			}
		} else {
			this.dimension.updateBlock(position);
		}
	}

	public addUpdate(block: Block): void {
		this.dimension.addUpdate(block);
	}

	public randomUpdate(): void {
		if (this.foreground.length > 0) {
			this.foreground[Math.floor(Math.random() * this.foreground.length)].randomUpdate();
		}
	}
}
