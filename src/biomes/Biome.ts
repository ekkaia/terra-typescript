import { BlockType } from "../blocks/BlockType";
import { Scope } from "../Data";
import Noise from "../Noise";

export abstract class Biome {
	protected heightMap: Noise;
	public readonly scope: Scope;
	public readonly name: string;
	public readonly floorBlock: BlockType;
	public readonly depthBlock: BlockType;
	public readonly minWidth: number;
	public readonly id: number;

	constructor(
		name: string,
		scope: Scope,
		baseHeight: number,
		minWidth: number,
		amplitude: number,
		waveLength: number,
		floorBlock: BlockType,
		depthBlock: BlockType
	) {
		this.name = name;
		this.scope = scope;
		this.heightMap = new Noise(baseHeight, amplitude, waveLength);
		this.floorBlock = floorBlock;
		this.depthBlock = depthBlock;
		this.minWidth = minWidth;
		this.id = Math.floor(Math.random() * 100);
	}

	public getHeightAtPosition(i: number): number {
		const spot = Math.abs(this.scope.to) - Math.abs(this.scope.from) - (Math.abs(this.scope.to) - Math.abs(i));
		const height = Math.trunc(this.heightMap.generate(spot));
		return height;
	}
}
