import { Biome } from "./Biome";
import { Scope } from "../Data";
import { Ice } from "../blocks/Ice";
import { IceTop } from "../blocks/IceTop";

export class Snow extends Biome {
	constructor(scope: Scope, baseHeight: number) {
		super("Snow", scope, baseHeight, 20, 20, 10, IceTop, Ice);
	}
}
