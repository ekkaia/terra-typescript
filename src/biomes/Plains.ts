import { Biome } from "./Biome";
import { Scope } from "../Data";
import { Dirt } from "../blocks/Dirt";
import { Grass } from "../blocks/Grass";

export class Plains extends Biome {
	constructor(scope: Scope, baseHeight: number) {
		super("Plain", scope, baseHeight, 20, 20, 10, Grass, Dirt);
	}
}
