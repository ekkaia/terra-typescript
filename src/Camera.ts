import { Position, Size } from "./Data";

export class Camera {
	public pos: Position;

	readonly size: Size;

	constructor(pos: Position = { x: 0, y: 0 }) {
		this.pos = pos;
		this.size = { w: window.innerWidth, h: window.innerHeight };
	}

	public left(i = 1): void {
		this.pos.x -= i;
	}

	public right(i = 1): void {
		this.pos.x += i;
	}

	public up(i = 1): void {
		this.pos.y -= i;
	}

	public down(i = 1): void {
		this.pos.y += i;
	}

	public getPos(): Position {
		return this.pos;
	}

	public getSize(): Size {
		return this.size;
	}
}
