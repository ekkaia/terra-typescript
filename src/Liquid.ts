import { Water } from "./blocks/Water";

export class Liquid {
	public volume: number;
	public quantity: number;
	public merged: Liquid;
	public water: Water[];
	//public needsTopUpdate: boolean;
	constructor(volume = 100, quantity = 1) {
		this.volume = volume;
		this.quantity = quantity;
		this.merged = undefined;
		this.water = [];
		//this.needsTopUpdate = false;
	}

	public add(water: Water): void {
		this.water.push(water);
	}

	public remove(water: Water): void {
		const findWater = (element: Water) => element === water;
		this.water.splice(this.water.findIndex(findWater), 1);
	}

	public blend(liquid: Liquid): void {
		//liquid.quantity += this.quantity;
		//liquid.volume += this.volume;
		if (this.merged != undefined) {
			this.merged = liquid;
			liquid.volume += this.volume;
			liquid.quantity += this.quantity;
		}
	}
}
