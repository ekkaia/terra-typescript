import { Scope } from "./Data";

export default class Noise {
	public amplitude: number;
	public waveLength: number;
	private readonly M = 4294967296;
	private readonly A = 1664525; // a-1 should be divisible by M's prime factors
	private readonly C = 1; // c and m should be co-prime
	private Z = Math.floor(Math.random() * this.M);
	private a = this.random();
	private b = this.random();
	private readonly baseHeight: number;
	public generation: Array<number> = [];

	constructor(baseHeight: number, amplitude: number, waveLength: number, size?: Scope) {
		this.baseHeight = baseHeight;
		this.amplitude = amplitude;
		this.waveLength = waveLength;

		if (size !== undefined) {
			for (let i = 0; i < Math.abs(size.to - size.from); i++) {
				this.generate(i);
			}
		}
	}

	private random(): number {
		this.Z = (this.A * this.Z + this.C) % this.M;
		return this.Z / this.M - 0.5;
	}

	private interpolate(pA: number, pB: number, pX: number): number {
		const ft = pX * Math.PI;
		const f = (1 - Math.cos(ft)) * 0.5;
		return pA * (1 - f) + pB * f;
	}

	public generate(i: number): number {
		let y = this.baseHeight;
		if (i % this.waveLength === 0) {
			this.a = this.b;
			this.b = this.random();
			y = this.baseHeight + this.a * this.amplitude;
		} else {
			y = this.baseHeight + this.interpolate(this.a, this.b, (i % this.waveLength) / this.waveLength) * this.amplitude;
		}
		this.generation.push(y);
		return y;
	}
}
